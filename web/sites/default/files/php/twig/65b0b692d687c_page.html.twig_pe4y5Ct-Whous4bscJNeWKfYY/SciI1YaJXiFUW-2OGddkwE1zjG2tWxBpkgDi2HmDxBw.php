<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/contrib/dempo/templates/system/page.html.twig */
class __TwigTemplate_d152c939c4e599aeb09eb84c2cc54076 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'navbar' => [$this, 'block_navbar'],
            'header' => [$this, 'block_header'],
            'main' => [$this, 'block_main'],
            'sidebar_first' => [$this, 'block_sidebar_first'],
            'highlighted' => [$this, 'block_highlighted'],
            'help' => [$this, 'block_help'],
            'content' => [$this, 'block_content'],
            'sidebar_second' => [$this, 'block_sidebar_second'],
            'footer' => [$this, 'block_footer'],
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $this->checkSecurity();
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 54
        echo "
";
        // line 56
        echo "
<div class=\"wrapper-version-";
        // line 57
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["design_home"] ?? null), 57, $this->source), "html", null, true);
        echo "\">

<button onclick=\"topFunction()\" id=\"myBtn\" title=\"Go to top\"><i class=\"fa fa-angle-up\"></i></button>
";
        // line 60
        $context["container"] = ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["theme"] ?? null), "settings", [], "any", false, false, true, 60), "fluid_container", [], "any", false, false, true, 60)) ? ("container-fluid") : ("container"));
        // line 62
        echo "<div class=\"header row\">
  ";
        // line 63
        $this->displayBlock('navbar', $context, $blocks);
        // line 227
        echo "</div>

";
        // line 230
        $this->displayBlock('main', $context, $blocks);
        // line 296
        echo "

";
        // line 298
        $this->displayBlock('footer', $context, $blocks);
        // line 335
        echo "
</div>";
        $this->env->getExtension('\Drupal\Core\Template\TwigExtension')
            ->checkDeprecations($context, ["design_home", "theme", "header_top_fluid_class", "page", "header_fluid_class", "design_front", "node", "content_attributes", "footer_top_fluid_class", "footer_fluid_class", "footer_bot_fluid_class"]);    }

    // line 63
    public function block_navbar($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 64
        echo "    ";
        // line 65
        $context["navbar_classes"] = ["navbar", ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,         // line 67
($context["theme"] ?? null), "settings", [], "any", false, false, true, 67), "navbar_inverse", [], "any", false, false, true, 67)) ? ("navbar-inverse") : ("navbar-default")), ((twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source,         // line 68
($context["theme"] ?? null), "settings", [], "any", false, false, true, 68), "navbar_position", [], "any", false, false, true, 68)) ? (("navbar-" . \Drupal\Component\Utility\Html::getClass($this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, ($context["theme"] ?? null), "settings", [], "any", false, false, true, 68), "navbar_position", [], "any", false, false, true, 68), 68, $this->source)))) : (""))];
        // line 71
        echo "    
    <div class=\"header-top-wrapper clearfix\">
      <div class=\"";
        // line 73
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["header_top_fluid_class"] ?? null), 73, $this->source), "html", null, true);
        echo "\">      
        ";
        // line 74
        if (twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "header_top", [], "any", false, false, true, 74)) {
            // line 75
            echo "          <div class=\"header-top\">
            ";
            // line 76
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "header_top", [], "any", false, false, true, 76), 76, $this->source), "html", null, true);
            echo "
          </div>
        ";
        }
        // line 79
        echo "      </div>
    </div>
    
    ";
        // line 83
        echo "        ";
        $this->displayBlock('header', $context, $blocks);
        // line 219
        echo "    
    ";
        // line 220
        if (twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "header_btm", [], "any", false, false, true, 220)) {
            // line 221
            echo "      <div class=\"header-btm col-12\">
        ";
            // line 222
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "header_btm", [], "any", false, false, true, 222), 222, $this->source), "html", null, true);
            echo "
      </div>
    ";
        }
        // line 225
        echo "    
  ";
    }

    // line 83
    public function block_header($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 84
        echo "          <div class=\"header-wrapper clearfix\" role=\"heading\">
            <div class=\"";
        // line 85
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["header_fluid_class"] ?? null), 85, $this->source), "html", null, true);
        echo "\">
              <div class=\"row header-inner align-items-center\">
                
                ";
        // line 88
        if (((($context["design_home"] ?? null) == "homepage-home-1") || ( !(is_string($__internal_compile_0 = ($context["design_home"] ?? null)) && is_string($__internal_compile_1 = "homepage-home-") && str_starts_with($__internal_compile_0, $__internal_compile_1)) && (($context["design_front"] ?? null) == "homepage-home-1")))) {
            // line 89
            echo "                    ";
            if (twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "branding", [], "any", false, false, true, 89)) {
                // line 90
                echo "                    <div class=\"branding col-md-3 col-8 order-md-1 order-1\">
                      ";
                // line 91
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "branding", [], "any", false, false, true, 91), 91, $this->source), "html", null, true);
                echo "
                    </div>
                    ";
            }
            // line 94
            echo "                    
                    ";
            // line 95
            if (twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "banner", [], "any", false, false, true, 95)) {
                // line 96
                echo "                      <div class=\"header col-md-9 col-12  order-md-2 order-4\">
                        ";
                // line 97
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "banner", [], "any", false, false, true, 97), 97, $this->source), "html", null, true);
                echo "
                      </div>
                    ";
            }
            // line 100
            echo "                    
                    ";
            // line 101
            if (twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "primary_menu", [], "any", false, false, true, 101)) {
                // line 102
                echo "                       <div class=\"dempo primary-menu col-md-11 col-2 order-md-3 order-3\">
                        ";
                // line 103
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "primary_menu", [], "any", false, false, true, 103), 103, $this->source), "html", null, true);
                echo "
                      </div>
                    ";
            }
            // line 106
            echo "                    
                    ";
            // line 107
            if (twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "search", [], "any", false, false, true, 107)) {
                // line 108
                echo "                      <div class=\"dempo search col-md-1 col-2 order-md-4 order-2\">
                        ";
                // line 109
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "search", [], "any", false, false, true, 109), 109, $this->source), "html", null, true);
                echo "
                      </div>
                    ";
            }
            // line 112
            echo "                
                ";
        } elseif (((        // line 113
($context["design_home"] ?? null) == "homepage-home-2") || ( !(is_string($__internal_compile_2 = ($context["design_home"] ?? null)) && is_string($__internal_compile_3 = "homepage-home-") && str_starts_with($__internal_compile_2, $__internal_compile_3)) && (($context["design_front"] ?? null) == "homepage-home-2")))) {
            // line 114
            echo "                
                    ";
            // line 115
            if (twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "banner", [], "any", false, false, true, 115)) {
                // line 116
                echo "                      <div class=\"header col-md-12 col-12 order-md-1 order-4\">
                        ";
                // line 117
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "banner", [], "any", false, false, true, 117), 117, $this->source), "html", null, true);
                echo "
                      </div>
                    ";
            }
            // line 120
            echo "                    
                    ";
            // line 121
            if (twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "branding", [], "any", false, false, true, 121)) {
                // line 122
                echo "                    <div class=\"branding col-md-12 col-8 justify-content-center d-flex order-md-2 order-1\">
                      ";
                // line 123
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "branding", [], "any", false, false, true, 123), 123, $this->source), "html", null, true);
                echo "
                    </div>
                    ";
            }
            // line 126
            echo "                    
                    ";
            // line 127
            if (twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "primary_menu", [], "any", false, false, true, 127)) {
                // line 128
                echo "                       <div class=\"dempo primary-menu col-md-9 col-2 order-md-2 order-3\">
                        ";
                // line 129
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "primary_menu", [], "any", false, false, true, 129), 129, $this->source), "html", null, true);
                echo "
                      </div>
                    ";
            }
            // line 132
            echo "                    
                    ";
            // line 133
            if (twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "search", [], "any", false, false, true, 133)) {
                // line 134
                echo "                      <div class=\"dempo search col-md-1 col-2 order-md-3 order-2\">
                        ";
                // line 135
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "search", [], "any", false, false, true, 135), 135, $this->source), "html", null, true);
                echo "
                      </div>
                    ";
            }
            // line 138
            echo "                
                ";
        } elseif (((        // line 139
($context["design_home"] ?? null) == "homepage-home-3") || ( !(is_string($__internal_compile_4 = ($context["design_home"] ?? null)) && is_string($__internal_compile_5 = "homepage-home-") && str_starts_with($__internal_compile_4, $__internal_compile_5)) && (($context["design_front"] ?? null) == "homepage-home-3")))) {
            // line 140
            echo "                
                    ";
            // line 141
            if (twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "banner", [], "any", false, false, true, 141)) {
                // line 142
                echo "                      <div class=\"header col-md-12 col-12 order-md-1 order-5\">
                        ";
                // line 143
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "banner", [], "any", false, false, true, 143), 143, $this->source), "html", null, true);
                echo "
                      </div>
                    ";
            }
            // line 146
            echo "                    
                    <div class=\"branding-left col-md-3 col-6 order-md-2 order-4\">";
            // line 147
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, twig_date_format_filter($this->env, "now", "l, F d, Y"), "html", null, true);
            echo "</div>
                    <div class=\"branding col-md-6 col-8 order-md-2 order-1\">";
            // line 148
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "branding", [], "any", false, false, true, 148), 148, $this->source), "html", null, true);
            echo "</div>
                    <div class=\"branding-right col-md-3 col-6 order-md-2 order-4\">";
            // line 149
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, twig_date_format_filter($this->env, "now", "l, F d, Y"), "html", null, true);
            echo "</div>
                    
                    ";
            // line 151
            if (twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "primary_menu", [], "any", false, false, true, 151)) {
                // line 152
                echo "                       <div class=\"dempo primary-menu col-md-11 col-2 order-md-3 order-3\">
                        ";
                // line 153
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "primary_menu", [], "any", false, false, true, 153), 153, $this->source), "html", null, true);
                echo "
                      </div>
                    ";
            }
            // line 156
            echo "                    
                    ";
            // line 157
            if (twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "search", [], "any", false, false, true, 157)) {
                // line 158
                echo "                      <div class=\"dempo search col-md-1 col-2 order-md-4 order-2\">
                        ";
                // line 159
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "search", [], "any", false, false, true, 159), 159, $this->source), "html", null, true);
                echo "
                      </div>
                    ";
            }
            // line 162
            echo "                
                ";
        } elseif (((        // line 163
($context["design_home"] ?? null) == "homepage-home-8") || ( !(is_string($__internal_compile_6 = ($context["design_home"] ?? null)) && is_string($__internal_compile_7 = "homepage-home-") && str_starts_with($__internal_compile_6, $__internal_compile_7)) && (($context["design_front"] ?? null) == "homepage-home-8")))) {
            // line 164
            echo "                
                    ";
            // line 165
            if (twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "banner", [], "any", false, false, true, 165)) {
                // line 166
                echo "                      <div class=\"header col-md-12 col-12 order-md-1 order-5\">
                        ";
                // line 167
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "banner", [], "any", false, false, true, 167), 167, $this->source), "html", null, true);
                echo "
                      </div>
                    ";
            }
            // line 170
            echo "                    
                    <div class=\"branding-left col-md-3 col-6 order-md-2 order-4\">";
            // line 171
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, twig_date_format_filter($this->env, "now", "l, F d, Y"), "html", null, true);
            echo "</div>
                    <div class=\"branding col-md-6 col-8 order-md-2 order-1\">";
            // line 172
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "branding", [], "any", false, false, true, 172), 172, $this->source), "html", null, true);
            echo "</div>
                    <div class=\"branding-right col-md-3 col-6 order-md-2 order-4\"><a href=\"";
            // line 173
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->renderVar($this->extensions['Drupal\Core\Template\TwigExtension']->getPath("view.news_latest.news_latest"));
            echo "\">";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->renderVar(t("Show All News"));
            echo "</a></div>
                    
                    ";
            // line 175
            if (twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "primary_menu", [], "any", false, false, true, 175)) {
                // line 176
                echo "                       <div class=\"dempo primary-menu col-md-11 col-2 order-md-3 order-3\">
                        ";
                // line 177
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "primary_menu", [], "any", false, false, true, 177), 177, $this->source), "html", null, true);
                echo "
                      </div>
                    ";
            }
            // line 180
            echo "                    
                    ";
            // line 181
            if (twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "search", [], "any", false, false, true, 181)) {
                // line 182
                echo "                      <div class=\"dempo search col-md-1 col-2 order-md-4 order-2\">
                        ";
                // line 183
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "search", [], "any", false, false, true, 183), 183, $this->source), "html", null, true);
                echo "
                      </div>
                    ";
            }
            // line 186
            echo "                    
                ";
        } else {
            // line 188
            echo "                
                    ";
            // line 189
            if (twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "branding", [], "any", false, false, true, 189)) {
                // line 190
                echo "                    <div class=\"branding col-md-3 col-8 order-md-1 order-1\">
                      ";
                // line 191
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "branding", [], "any", false, false, true, 191), 191, $this->source), "html", null, true);
                echo "
                    </div>
                    ";
            }
            // line 194
            echo "                    
                    ";
            // line 195
            if (twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "banner", [], "any", false, false, true, 195)) {
                // line 196
                echo "                      <div class=\"header col-md-9 col-12  order-md-2 order-4\">
                        ";
                // line 197
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "banner", [], "any", false, false, true, 197), 197, $this->source), "html", null, true);
                echo "
                      </div>
                    ";
            }
            // line 200
            echo "                    
                    ";
            // line 201
            if (twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "primary_menu", [], "any", false, false, true, 201)) {
                // line 202
                echo "                       <div class=\"dempo primary-menu col-md-11 col-2 order-md-3 order-3\">
                        ";
                // line 203
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "primary_menu", [], "any", false, false, true, 203), 203, $this->source), "html", null, true);
                echo "
                      </div>
                    ";
            }
            // line 206
            echo "                    
                    ";
            // line 207
            if (twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "search", [], "any", false, false, true, 207)) {
                // line 208
                echo "                      <div class=\"dempo search col-md-1 col-2 order-md-4 order-2\">
                        ";
                // line 209
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "search", [], "any", false, false, true, 209), 209, $this->source), "html", null, true);
                echo "
                      </div>
                    ";
            }
            // line 212
            echo "                    
                ";
        }
        // line 214
        echo "                
              </div>
            </div>
          </div>
        ";
    }

    // line 230
    public function block_main($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 231
        echo "
  ";
        // line 232
        $context["node_type"] = \Drupal\Component\Utility\Html::getClass($this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["node"] ?? null), "bundle", [], "any", false, false, true, 232), 232, $this->source));
        // line 233
        echo "  ";
        if ((($context["node_type"] ?? null) == "homepage")) {
            // line 234
            echo "    <div role=\"main\" class=\"main-container container-fluid js-quickedit-main-content\">
  ";
        } else {
            // line 236
            echo "    <div role=\"main\" class=\"main-container container       js-quickedit-main-content\">
  ";
        }
        // line 238
        echo "  
    <div class=\"row main-container-inner ";
        // line 239
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["node_type"] ?? null), 239, $this->source), "html", null, true);
        echo "\">
      ";
        // line 240
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "breadcrumb", [], "any", false, false, true, 240), 240, $this->source), "html", null, true);
        echo "
      
      ";
        // line 243
        echo "      ";
        if (twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "sidebar_first", [], "any", false, false, true, 243)) {
            // line 244
            echo "        ";
            $this->displayBlock('sidebar_first', $context, $blocks);
            // line 249
            echo "      ";
        }
        // line 250
        echo "
      ";
        // line 252
        echo "      
      ";
        // line 254
        $context["content_classes"] = [(((twig_get_attribute($this->env, $this->source,         // line 255
($context["page"] ?? null), "sidebar_first", [], "any", false, false, true, 255) && twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "sidebar_second", [], "any", false, false, true, 255))) ? ("col-sm-6") : ("")), (((twig_get_attribute($this->env, $this->source,         // line 256
($context["page"] ?? null), "sidebar_first", [], "any", false, false, true, 256) && twig_test_empty(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "sidebar_second", [], "any", false, false, true, 256)))) ? ("col-sm-9") : ("")), (((twig_get_attribute($this->env, $this->source,         // line 257
($context["page"] ?? null), "sidebar_second", [], "any", false, false, true, 257) && twig_test_empty(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "sidebar_first", [], "any", false, false, true, 257)))) ? ("col-sm-9") : ("")), (((twig_test_empty(twig_get_attribute($this->env, $this->source,         // line 258
($context["page"] ?? null), "sidebar_first", [], "any", false, false, true, 258)) && twig_test_empty(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "sidebar_second", [], "any", false, false, true, 258)))) ? ("col-sm-12xxx") : (""))];
        // line 261
        echo "
      <section";
        // line 262
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["content_attributes"] ?? null), "addClass", [($context["content_classes"] ?? null)], "method", false, false, true, 262), 262, $this->source), "html", null, true);
        echo ">

        ";
        // line 265
        echo "        ";
        if (twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "highlighted", [], "any", false, false, true, 265)) {
            // line 266
            echo "          ";
            $this->displayBlock('highlighted', $context, $blocks);
            // line 269
            echo "        ";
        }
        // line 270
        echo "
        ";
        // line 272
        echo "        ";
        if (twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "help", [], "any", false, false, true, 272)) {
            // line 273
            echo "          ";
            $this->displayBlock('help', $context, $blocks);
            // line 276
            echo "        ";
        }
        // line 277
        echo "
        ";
        // line 279
        echo "        ";
        $this->displayBlock('content', $context, $blocks);
        // line 283
        echo "      </section>

      ";
        // line 286
        echo "      ";
        if (twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "sidebar_second", [], "any", false, false, true, 286)) {
            // line 287
            echo "        ";
            $this->displayBlock('sidebar_second', $context, $blocks);
            // line 292
            echo "      ";
        }
        // line 293
        echo "    </div>
  </div>
";
    }

    // line 244
    public function block_sidebar_first($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 245
        echo "          <aside class=\"col-sm-3\" role=\"complementary\">
            ";
        // line 246
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "sidebar_first", [], "any", false, false, true, 246), 246, $this->source), "html", null, true);
        echo "
          </aside>
        ";
    }

    // line 266
    public function block_highlighted($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 267
        echo "            <div class=\"highlighted container\">";
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "highlighted", [], "any", false, false, true, 267), 267, $this->source), "html", null, true);
        echo "</div>
          ";
    }

    // line 273
    public function block_help($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 274
        echo "            ";
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "help", [], "any", false, false, true, 274), 274, $this->source), "html", null, true);
        echo "
          ";
    }

    // line 279
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 280
        echo "          <a id=\"main-content\"></a>
          ";
        // line 281
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "content", [], "any", false, false, true, 281), 281, $this->source), "html", null, true);
        echo "
        ";
    }

    // line 287
    public function block_sidebar_second($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 288
        echo "          <aside class=\"col-sm-3\" role=\"complementary\">
            ";
        // line 289
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "sidebar_second", [], "any", false, false, true, 289), 289, $this->source), "html", null, true);
        echo "
          </aside>
        ";
    }

    // line 298
    public function block_footer($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 299
        echo "    
    <footer class=\"footer\" role=\"contentinfo\">
      
      <div class=\"footer_top-wrapper clearfix\">
        <div class=\"";
        // line 303
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["footer_top_fluid_class"] ?? null), 303, $this->source), "html", null, true);
        echo "\">
          <div class=\"row row-no-gutters\">
            ";
        // line 305
        if (twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "footer_top", [], "any", false, false, true, 305)) {
            // line 306
            echo "                ";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "footer_top", [], "any", false, false, true, 306), 306, $this->source), "html", null, true);
            echo "
            ";
        }
        // line 308
        echo "          </div>
        </div>
      </div>
      
      <div class=\"footer_wrapper clearfix\">
        <div class=\"";
        // line 313
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["footer_fluid_class"] ?? null), 313, $this->source), "html", null, true);
        echo "\">
          <div class=\"row row-no-gutters\">
            ";
        // line 315
        if (twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "footer", [], "any", false, false, true, 315)) {
            // line 316
            echo "                ";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "footer", [], "any", false, false, true, 316), 316, $this->source), "html", null, true);
            echo "
            ";
        }
        // line 318
        echo "          </div>
        </div>
      </div>
      
      <div class=\"footer_bot-wrapper clearfix\">
        <div class=\"";
        // line 323
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["footer_bot_fluid_class"] ?? null), 323, $this->source), "html", null, true);
        echo "\">
          <div class=\"row row-no-gutters\">
            ";
        // line 325
        if (twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "footer_bot", [], "any", false, false, true, 325)) {
            // line 326
            echo "                ";
            echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["page"] ?? null), "footer_bot", [], "any", false, false, true, 326), 326, $this->source), "html", null, true);
            echo "
            ";
        }
        // line 328
        echo "          </div>
        </div>
      </div>

    </footer>

";
    }

    /**
     * @codeCoverageIgnore
     */
    public function getTemplateName()
    {
        return "themes/contrib/dempo/templates/system/page.html.twig";
    }

    /**
     * @codeCoverageIgnore
     */
    public function isTraitable()
    {
        return false;
    }

    /**
     * @codeCoverageIgnore
     */
    public function getDebugInfo()
    {
        return array (  698 => 328,  692 => 326,  690 => 325,  685 => 323,  678 => 318,  672 => 316,  670 => 315,  665 => 313,  658 => 308,  652 => 306,  650 => 305,  645 => 303,  639 => 299,  635 => 298,  628 => 289,  625 => 288,  621 => 287,  615 => 281,  612 => 280,  608 => 279,  601 => 274,  597 => 273,  590 => 267,  586 => 266,  579 => 246,  576 => 245,  572 => 244,  566 => 293,  563 => 292,  560 => 287,  557 => 286,  553 => 283,  550 => 279,  547 => 277,  544 => 276,  541 => 273,  538 => 272,  535 => 270,  532 => 269,  529 => 266,  526 => 265,  521 => 262,  518 => 261,  516 => 258,  515 => 257,  514 => 256,  513 => 255,  512 => 254,  509 => 252,  506 => 250,  503 => 249,  500 => 244,  497 => 243,  492 => 240,  488 => 239,  485 => 238,  481 => 236,  477 => 234,  474 => 233,  472 => 232,  469 => 231,  465 => 230,  457 => 214,  453 => 212,  447 => 209,  444 => 208,  442 => 207,  439 => 206,  433 => 203,  430 => 202,  428 => 201,  425 => 200,  419 => 197,  416 => 196,  414 => 195,  411 => 194,  405 => 191,  402 => 190,  400 => 189,  397 => 188,  393 => 186,  387 => 183,  384 => 182,  382 => 181,  379 => 180,  373 => 177,  370 => 176,  368 => 175,  361 => 173,  357 => 172,  353 => 171,  350 => 170,  344 => 167,  341 => 166,  339 => 165,  336 => 164,  334 => 163,  331 => 162,  325 => 159,  322 => 158,  320 => 157,  317 => 156,  311 => 153,  308 => 152,  306 => 151,  301 => 149,  297 => 148,  293 => 147,  290 => 146,  284 => 143,  281 => 142,  279 => 141,  276 => 140,  274 => 139,  271 => 138,  265 => 135,  262 => 134,  260 => 133,  257 => 132,  251 => 129,  248 => 128,  246 => 127,  243 => 126,  237 => 123,  234 => 122,  232 => 121,  229 => 120,  223 => 117,  220 => 116,  218 => 115,  215 => 114,  213 => 113,  210 => 112,  204 => 109,  201 => 108,  199 => 107,  196 => 106,  190 => 103,  187 => 102,  185 => 101,  182 => 100,  176 => 97,  173 => 96,  171 => 95,  168 => 94,  162 => 91,  159 => 90,  156 => 89,  154 => 88,  148 => 85,  145 => 84,  141 => 83,  136 => 225,  130 => 222,  127 => 221,  125 => 220,  122 => 219,  119 => 83,  114 => 79,  108 => 76,  105 => 75,  103 => 74,  99 => 73,  95 => 71,  93 => 68,  92 => 67,  91 => 65,  89 => 64,  85 => 63,  79 => 335,  77 => 298,  73 => 296,  71 => 230,  67 => 227,  65 => 63,  62 => 62,  60 => 60,  54 => 57,  51 => 56,  48 => 54,);
    }

    public function getSourceContext()
    {
        return new Source("", "themes/contrib/dempo/templates/system/page.html.twig", "/var/www/html/web/themes/contrib/dempo/templates/system/page.html.twig");
    }
    
    public function checkSecurity()
    {
        static $tags = array("set" => 60, "block" => 63, "if" => 74);
        static $filters = array("escape" => 57, "clean_class" => 68, "date" => 147, "t" => 173);
        static $functions = array("path" => 173);

        try {
            $this->sandbox->checkSecurity(
                ['set', 'block', 'if'],
                ['escape', 'clean_class', 'date', 't'],
                ['path']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }
}
