<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/contrib/dempo/templates/navigation/menu--main.html.twig */
class __TwigTemplate_429a8d000dcd392ea45f5444f65a6827 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $this->checkSecurity();
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 24
        echo "<ul";
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["attributes"] ?? null), "addClass", ["navbar-nav flex-grow-1 pe-3", "dempo-navbar-nav"], "method", false, false, true, 24), 24, $this->source), "html", null, true);
        echo ">
    ";
        // line 25
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["items"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
            // line 26
            echo "      ";
            // line 27
            $context["classes"] = ["nav-item", ((twig_get_attribute($this->env, $this->source,             // line 29
$context["item"], "is_expanded", [], "any", false, false, true, 29)) ? ("menu-item--expanded") : ("")), ((twig_get_attribute($this->env, $this->source,             // line 30
$context["item"], "is_collapsed", [], "any", false, false, true, 30)) ? ("menu-item--collapsed") : ("")), ((twig_get_attribute($this->env, $this->source,             // line 31
$context["item"], "in_active_trail", [], "any", false, false, true, 31)) ? ("menu-item--active-trail") : (""))];
            // line 34
            echo "      
      ";
            // line 35
            $context["coba"] = [((twig_get_attribute($this->env, $this->source, $context["item"], "in_active_trail", [], "any", false, false, true, 35)) ? ("menu-item--active-trail") : (""))];
            // line 36
            echo "      
      ";
            // line 37
            if (twig_get_attribute($this->env, $this->source, $context["item"], "below", [], "any", false, false, true, 37)) {
                // line 38
                echo "        <li class=\"nav-item dropdown nav-level-";
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["menu_level"] ?? null), 38, $this->source), "html", null, true);
                echo "\">
            ";
                // line 40
                echo "            
            <span data-bs-hover =\"dropdown\" class=\"d-none d-sm-block\">";
                // line 41
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->extensions['Drupal\Core\Template\TwigExtension']->getLink($this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["item"], "title", [], "any", false, false, true, 41), 41, $this->source), $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["item"], "url", [], "any", false, false, true, 41), 41, $this->source), ["class" => ["nav-link dropdown-toggle"]]), "html", null, true);
                echo "</span>
            <span data-bs-toggle=\"dropdown\" class=\"d-block d-sm-none\">";
                // line 42
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->extensions['Drupal\Core\Template\TwigExtension']->getLink($this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["item"], "title", [], "any", false, false, true, 42), 42, $this->source), $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["item"], "url", [], "any", false, false, true, 42), 42, $this->source), ["class" => ["nav-link dropdown-toggle"]]), "html", null, true);
                echo "</span>
            
            ";
                // line 44
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, ($context["menus"] ?? null), "menu_links", [twig_get_attribute($this->env, $this->source, $context["item"], "below", [], "any", false, false, true, 44), ($context["attributes"] ?? null), (($context["menu_level"] ?? null) + 1)], "method", false, false, true, 44), 44, $this->source), "html", null, true);
                echo "
            
            ";
                // line 47
                echo "            
            <ul class=\"dropdown-menu\">
                <li><a class=\"dropdown-item d-block d-sm-none\" href=\"";
                // line 49
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["item"], "url", [], "any", false, false, true, 49), 49, $this->source), "html", null, true);
                echo "\">";
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["item"], "title", [], "any", false, false, true, 49), 49, $this->source), "html", null, true);
                echo "</a></li>
                ";
                // line 50
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->source, $context["item"], "below", [], "any", false, false, true, 50));
                foreach ($context['_seq'] as $context["_key"] => $context["subitem1"]) {
                    // line 51
                    echo "                    ";
                    echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->extensions['Drupal\Core\Template\TwigExtension']->getLink($this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["subitem1"], "title", [], "any", false, false, true, 51), 51, $this->source), $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["subitem1"], "url", [], "any", false, false, true, 51), 51, $this->source), ["class" => ["dropdown-item"]]), "html", null, true);
                    echo "
                ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['subitem1'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 53
                echo "            </ul>
        </li>
            

      ";
            } else {
                // line 58
                echo "        <li class=\"nav-item nav-level-";
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["menu_level"] ?? null), 58, $this->source), "html", null, true);
                echo "\">
            ";
                // line 59
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->extensions['Drupal\Core\Template\TwigExtension']->getLink($this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["item"], "title", [], "any", false, false, true, 59), 59, $this->source), $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["item"], "url", [], "any", false, false, true, 59), 59, $this->source), ["class" => ["nav-link"]]), "html", null, true);
                echo "
        </li>
      ";
            }
            // line 62
            echo "    
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 64
        echo "</ul>
";
        $this->env->getExtension('\Drupal\Core\Template\TwigExtension')
            ->checkDeprecations($context, ["attributes", "items", "menu_level", "menus"]);    }

    /**
     * @codeCoverageIgnore
     */
    public function getTemplateName()
    {
        return "themes/contrib/dempo/templates/navigation/menu--main.html.twig";
    }

    /**
     * @codeCoverageIgnore
     */
    public function isTraitable()
    {
        return false;
    }

    /**
     * @codeCoverageIgnore
     */
    public function getDebugInfo()
    {
        return array (  135 => 64,  128 => 62,  122 => 59,  117 => 58,  110 => 53,  101 => 51,  97 => 50,  91 => 49,  87 => 47,  82 => 44,  77 => 42,  73 => 41,  70 => 40,  65 => 38,  63 => 37,  60 => 36,  58 => 35,  55 => 34,  53 => 31,  52 => 30,  51 => 29,  50 => 27,  48 => 26,  44 => 25,  39 => 24,);
    }

    public function getSourceContext()
    {
        return new Source("", "themes/contrib/dempo/templates/navigation/menu--main.html.twig", "/var/www/html/web/themes/contrib/dempo/templates/navigation/menu--main.html.twig");
    }
    
    public function checkSecurity()
    {
        static $tags = array("for" => 25, "set" => 27, "if" => 37);
        static $filters = array("escape" => 24);
        static $functions = array("link" => 41);

        try {
            $this->sandbox->checkSecurity(
                ['for', 'set', 'if'],
                ['escape'],
                ['link']
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }
}
