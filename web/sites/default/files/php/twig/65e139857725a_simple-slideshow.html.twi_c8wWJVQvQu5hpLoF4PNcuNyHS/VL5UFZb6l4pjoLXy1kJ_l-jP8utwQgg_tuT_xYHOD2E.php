<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* modules/contrib/simple_slideshow/templates/simple-slideshow.html.twig */
class __TwigTemplate_058a1ac3421cb2100877538423442d07 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $this->checkSecurity();
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 16
        echo "
<div class=\"simple_slideshow-wrapper\">
    <div class=\"splide\" data-splide='{  \"type\":\"";
        // line 18
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["simple_slideshow_type"] ?? null), 18, $this->source), "html", null, true);
        echo "\",
                                        \"autoplay\":";
        // line 19
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["simple_slideshow_autoplay"] ?? null), 19, $this->source), "html", null, true);
        echo ",
                                        \"rewind\":";
        // line 20
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["simple_slideshow_rewind"] ?? null), 20, $this->source), "html", null, true);
        echo ",
                                        \"speed\":";
        // line 21
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["simple_slideshow_speed"] ?? null), 21, $this->source), "html", null, true);
        echo ",
                                        \"start\":";
        // line 22
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["simple_slideshow_start"] ?? null), 22, $this->source), "html", null, true);
        echo ",
                                        \"perPage\":";
        // line 23
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["simple_slideshow_perpage"] ?? null), 23, $this->source), "html", null, true);
        echo ",
                                        \"perMove\":";
        // line 24
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["simple_slideshow_permove"] ?? null), 24, $this->source), "html", null, true);
        echo ",
                                        \"gap\":\"";
        // line 25
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["simple_slideshow_gap"] ?? null), 25, $this->source), "html", null, true);
        echo "\",
                                        \"padding\":\"";
        // line 26
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["simple_slideshow_padding"] ?? null), 26, $this->source), "html", null, true);
        echo "\",
                                        \"arrows\":";
        // line 27
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["simple_slideshow_arrows"] ?? null), 27, $this->source), "html", null, true);
        echo ",
                                        \"pagination\":";
        // line 28
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["simple_slideshow_pagination"] ?? null), 28, $this->source), "html", null, true);
        echo ",
                                        \"arrowPath\":\"";
        // line 29
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["simple_slideshow_arrowpath"] ?? null), 29, $this->source), "html", null, true);
        echo "\",
                                        \"pauseOnHover\":";
        // line 30
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["simple_slideshow_pausehover"] ?? null), 30, $this->source), "html", null, true);
        echo ",
                                        \"lazyLoad\":";
        // line 31
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["simple_slideshow_lazyload"] ?? null), 31, $this->source), "html", null, true);
        echo ",
                                        \"direction\":\"";
        // line 32
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["simple_slideshow_direction"] ?? null), 32, $this->source), "html", null, true);
        echo "\"
                                     }'>
        <div class=\"splide__track\">
            
    \t\t<ul class=\"splide__list coba ";
        // line 36
        echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["image_alt"] ?? null), 36, $this->source), "html", null, true);
        echo "\">
    \t\t    
            ";
        // line 38
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["url"] ?? null));
        foreach ($context['_seq'] as $context["key"] => $context["value"]) {
            // line 39
            echo "                ";
            $context["image_alt"] = twig_get_attribute($this->env, $this->source, $context["value"], "alt", [], "any", false, false, true, 39);
            // line 40
            echo "                ";
            if (((($context["simple_slideshow_linkimgalt"] ?? null) == "linkimgalt") && (($context["image_alt"] ?? null) != ""))) {
                // line 41
                echo "                    <li class=\"splide__slide\"><a href=\"";
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["value"], "alt", [], "any", false, false, true, 41), 41, $this->source), "html", null, true);
                echo "\"><img title=\"";
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["value"], "title", [], "any", false, false, true, 41), 41, $this->source), "html", null, true);
                echo "\" alt=\"";
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["value"], "alt", [], "any", false, false, true, 41), 41, $this->source), "html", null, true);
                echo "\" src=\"";
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["value"], "uri", [], "any", false, false, true, 41), 41, $this->source), "html", null, true);
                echo "\" onclick=\"window.open('";
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["value"], "alt", [], "any", false, false, true, 41), 41, $this->source), "html", null, true);
                echo "', '_self')\" /></a></li>
                ";
            } elseif ((            // line 42
($context["simple_slideshow_linkimgalt"] ?? null) == "file")) {
                // line 43
                echo "                    <li class=\"splide__slide\"><img title=\"";
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["value"], "title", [], "any", false, false, true, 43), 43, $this->source), "html", null, true);
                echo "\" alt=\"";
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["value"], "alt", [], "any", false, false, true, 43), 43, $this->source), "html", null, true);
                echo "\" src=\"";
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["value"], "uri", [], "any", false, false, true, 43), 43, $this->source), "html", null, true);
                echo "\" onclick=\"window.open('";
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["value"], "uri", [], "any", false, false, true, 43), 43, $this->source), "html", null, true);
                echo "', '_self')\" /></li>
                ";
            } else {
                // line 45
                echo "                    <li class=\"splide__slide\"><img title=\"";
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["value"], "title", [], "any", false, false, true, 45), 45, $this->source), "html", null, true);
                echo "\" alt=\"";
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["value"], "alt", [], "any", false, false, true, 45), 45, $this->source), "html", null, true);
                echo "\" src=\"";
                echo $this->extensions['Drupal\Core\Template\TwigExtension']->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(twig_get_attribute($this->env, $this->source, $context["value"], "uri", [], "any", false, false, true, 45), 45, $this->source), "html", null, true);
                echo "\" /></li>
                ";
            }
            // line 47
            echo "            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['value'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 48
        echo "            </ul>
        </div>
    </div>
</div>
";
        $this->env->getExtension('\Drupal\Core\Template\TwigExtension')
            ->checkDeprecations($context, ["simple_slideshow_type", "simple_slideshow_autoplay", "simple_slideshow_rewind", "simple_slideshow_speed", "simple_slideshow_start", "simple_slideshow_perpage", "simple_slideshow_permove", "simple_slideshow_gap", "simple_slideshow_padding", "simple_slideshow_arrows", "simple_slideshow_pagination", "simple_slideshow_arrowpath", "simple_slideshow_pausehover", "simple_slideshow_lazyload", "simple_slideshow_direction", "image_alt", "url", "simple_slideshow_linkimgalt"]);    }

    /**
     * @codeCoverageIgnore
     */
    public function getTemplateName()
    {
        return "modules/contrib/simple_slideshow/templates/simple-slideshow.html.twig";
    }

    /**
     * @codeCoverageIgnore
     */
    public function isTraitable()
    {
        return false;
    }

    /**
     * @codeCoverageIgnore
     */
    public function getDebugInfo()
    {
        return array (  164 => 48,  158 => 47,  148 => 45,  136 => 43,  134 => 42,  121 => 41,  118 => 40,  115 => 39,  111 => 38,  106 => 36,  99 => 32,  95 => 31,  91 => 30,  87 => 29,  83 => 28,  79 => 27,  75 => 26,  71 => 25,  67 => 24,  63 => 23,  59 => 22,  55 => 21,  51 => 20,  47 => 19,  43 => 18,  39 => 16,);
    }

    public function getSourceContext()
    {
        return new Source("", "modules/contrib/simple_slideshow/templates/simple-slideshow.html.twig", "/var/www/html/web/modules/contrib/simple_slideshow/templates/simple-slideshow.html.twig");
    }
    
    public function checkSecurity()
    {
        static $tags = array("for" => 38, "set" => 39, "if" => 40);
        static $filters = array("escape" => 18);
        static $functions = array();

        try {
            $this->sandbox->checkSecurity(
                ['for', 'set', 'if'],
                ['escape'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->source);

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }
}
