<?php

/**
 * @file
 * This module provides the form element for the Contact module.
 */

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\mosparo_contact\Plugin\Field\FieldWidget\MosparoWidget;
use Drupal\mosparo_integration\MosparoConnectionInterface;
use Mosparo\ApiClient\Exception;

/**
 * Validates the form.
 *
 * @param array $form
 *   The array with the full form.
 * @param \Drupal\Core\Form\FormStateInterface $form_state
 *   The state of the form.
 * @param \Drupal\mosparo_integration\MosparoConnectionInterface $connection
 *   The mosparo connection.
 * @param \Drupal\mosparo_contact\Plugin\Field\FieldWidget\MosparoWidget $widget
 *   The form widget.
 *
 * @return bool
 *   TRUE if the form is submittable, or FALSE if there was an error.
 */
function mosparo_contact_form_validate(array $form, FormStateInterface &$form_state, MosparoConnectionInterface $connection, MosparoWidget $widget) {
  $mosparoHelper = \Drupal::service('mosparo_integration.service');
  $fieldInformation = $mosparoHelper->extractFieldInformation($form_state);

  [
    $formData,
    $requiredFields,
    $verifiableFields,
    $submitToken,
    $validationToken,
  ] = $mosparoHelper->prepareFormData(
    $form_state->getUserInput(),
    $fieldInformation
  );

  $form_error_handler = \Drupal::service('form_error_handler');

  $client = $mosparoHelper->getApiClient($connection);

  try {
    /** @var \Mosparo\ApiClient\VerificationResult $res */
    $res = $client->verifySubmission($formData, $submitToken, $validationToken);

    if ($res !== NULL) {
      $verifiedFields = array_keys($res->getVerifiedFields());
      $requiredFieldDifference = array_diff($requiredFields, $verifiedFields);
      $verifiableFieldDifference = array_diff($verifiableFields, $verifiedFields);

      if ($res->isSubmittable() && empty($requiredFieldDifference) && empty($verifiableFieldDifference)) {
        return TRUE;
      }
      else {
        $form_state->setErrorByName($widget->getBaseId(), t('The form is not submittable.'));
        $form_error_handler->handleFormErrors($form, $form_state);
        $form_state->setValidationComplete();
      }
    }
    else {
      foreach ($res->getIssues() as $issue) {
        $form_state->setErrorByName($widget->getBaseId(), t('mosparo returned an error. Error: :message', [':message' => $issue['message']]));
        $form_error_handler->handleFormErrors($form, $form_state);
        $form_state->setValidationComplete();
      }
    }
  }
  catch (Exception $e) {
    $form_state->setErrorByName($widget->getBaseId(), $e->getMessage());
    $form_error_handler->handleFormErrors($form, $form_state);
    $form_state->setValidationComplete();
  }

  return FALSE;
}

/**
 * Drupal hook: hook_help()
 */
function mosparo_contact_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {

    case 'help.page.mosparo_contact':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('Add mosparo, the modern spam protection system, to your Contact form.') . '</p>';
      $output .= '<p>';
      $output .= t('Visit the <a href=":project_link">mosparo integration project page</a> on Drupal.org for more information.', [
        ':project_link' => 'https://www.drupal.org/project/mosparo_integration',
      ]);
      $output .= '</p>';

      return $output;

    default:
  }
}
