<?php

namespace Drupal\mosparo_contact\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'mosparo_default' widget.
 *
 * @FieldWidget(
 *   id = "mosparo_default",
 *   label = @Translation("mosparo"),
 *   field_types = {
 *     "mosparo_contact"
 *   }
 * )
 */
class MosparoWidget extends WidgetBase {

  /**
   * The EntityStorage for the mosparo connection entities.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $entityStorage;

  /**
   * The mosparo service to help with the mosparo integration.
   *
   * @var \Drupal\mosparo_integration\Service\MosparoService
   */
  protected $mosparoService;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);

    $instance->entityStorage = $container->get('entity_type.manager')->getStorage('mosparo_connection');
    $instance->mosparoService = $container->get('mosparo_integration.service');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\mosparo_integration\MosparoConnectionInterface $connection */
    $connection = $this->entityStorage->load($this->getFieldSetting('mosparo_connection'));

    $designMode = FALSE;
    $buildInfo = $form_state->getBuildInfo();
    if (isset($buildInfo['base_form_id']) && $buildInfo['base_form_id'] === 'field_config_form') {
      $designMode = TRUE;
    }

    $fieldData = [];
    if ($connection !== NULL) {
      $attached = $this->mosparoService->generateAttached($connection);

      $fieldData = [
        '#markup' => $this->mosparoService->generateHtml($connection, $designMode),
        '#attached' => $attached,
      ];
    }

    $form['#validate'][] = function ($form, $form_state) use ($connection) {
      mosparo_contact_form_validate($form, $form_state, $connection, $this);
    };

    $element['value'] = $element + $fieldData;

    return $element;
  }

}
