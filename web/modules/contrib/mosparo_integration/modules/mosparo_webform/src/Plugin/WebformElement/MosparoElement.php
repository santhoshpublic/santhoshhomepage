<?php

namespace Drupal\mosparo_webform\Plugin\WebformElement;

use Drupal\Core\Form\FormStateInterface;
use Drupal\webform\Plugin\WebformElementBase;
use Drupal\webform\WebformSubmissionInterface;
use Mosparo\ApiClient\Exception;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides an webform element to add mosparo to a webform form.
 *
 * @WebformElement(
 *   id = "mosparo_webform",
 *   default_key = "mosparo",
 *   label = @Translation("mosparo"),
 *   category = @Translation("Advanced elements"),
 *   description = @Translation("Provides an element to protect your form with mosparo."),
 * )
 */
class MosparoElement extends WebformElementBase {

  /**
   * The EntityStorage for the mosparo connection entities.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $entityStorage;

  /**
   * The mosparo service to help with the mosparo integration.
   *
   * @var \Drupal\mosparo_integration\Service\MosparoService
   */
  protected $mosparoService;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);

    $instance->entityStorage = $container->get('entity_type.manager')->getStorage('mosparo_connection');
    $instance->mosparoService = $container->get('mosparo_integration.service');

    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  protected function defineDefaultProperties() {
    return [
      'mosparo_connection' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    return [
      '#input' => TRUE,
      '#size' => 60,
      '#required' => FALSE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = [];
    $form['mosparo'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('mosparo Settings'),
    ];

    $options = [];
    $ids = $this->entityStorage->getQuery()->execute();
    $connections = $this->entityStorage->loadMultiple($ids);
    foreach ($connections as $connection) {
      $options[$connection->getId()] = $connection->getLabel();
    }

    $form['mosparo']['mosparo_connection'] = [
      '#type' => 'select',
      '#title' => $this->t('mosparo Connection'),
      '#options' => $options,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function prepare(array &$element, WebformSubmissionInterface $webform_submission = NULL) {
    parent::prepare($element, $webform_submission);

    $connection = $this->entityStorage->load($element['#mosparo_connection']);

    $fieldData = [];
    if ($connection !== NULL) {
      $attached = $this->mosparoService->generateAttached($connection);

      $fieldData = [
        '#markup' => $this->mosparoService->generateHtml($connection),
        '#attached' => $attached,
      ];
    }

    $element['#element_validate'][] = [$this, 'validateMosparoForm'];

    $element['value'] = $fieldData;
  }

  /**
   * Verifies the submitted form data with the help of mosparo.
   *
   * @param array $element
   *   The mosparo element.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The state of the form.
   * @param array $complete_form
   *   The complete form as array.
   */
  public function validateMosparoForm(array &$element, FormStateInterface $form_state, array &$complete_form) {
    /** @var \Drupal\mosparo_integration\MosparoConnectionInterface $connection */
    $connection = $this->entityStorage->load($element['#mosparo_connection']);
    $fieldInformation = $this->mosparoService->extractFieldInformation($form_state);

    [
      $formData,
      $requiredFields,
      $verifiableFields,
      $submitToken,
      $validationToken,
    ] = $this->mosparoService->prepareFormData(
      $form_state->getUserInput(),
      $fieldInformation,
      ['captcha', 'captcha_sid', 'captcha_token', 'captcha_cacheable']
    );

    $client = $this->mosparoService->getApiClient($connection);

    try {
      /** @var \Mosparo\ApiClient\VerificationResult $res */
      $res = $client->verifySubmission($formData, $submitToken, $validationToken);

      if ($res !== NULL) {
        $verifiedFields = array_keys($res->getVerifiedFields());
        $requiredFieldDifference = array_diff($requiredFields, $verifiedFields);
        $verifiableFieldDifference = array_diff($verifiableFields, $verifiedFields);

        if ($res->isSubmittable() && empty($requiredFieldDifference) && empty($verifiableFieldDifference)) {
          return;
        }
        else {
          $form_state->setError($element, $this->t('The form is not submittable.'));
        }
      }
      else {
        foreach ($res->getIssues() as $issue) {
          $form_state->setError($element, $this->t('mosparo returned an error. Error: %message', ['%message' => $issue['message']]));
        }
      }
    }
    catch (Exception $e) {
      $form_state->setError($element, $e->getMessage());
    }
  }

}
