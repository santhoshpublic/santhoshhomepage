<?php

namespace Drupal\Tests\mosparo_integration\Unit\Event;

use Drupal\mosparo_integration\Event\MosparoIntegrationFilterFieldTypesEvent;
use Drupal\Tests\UnitTestCase;

/**
 * Tests the mosparo Filter Field Types Event.
 *
 * @coversDefaultClass \Drupal\mosparo_integration\Event\MosparoIntegrationFilterFieldTypesEvent
 *
 * @group mosparo_integration
 */
class MosparoIntegrationFilterFieldTypesEventTest extends UnitTestCase {

  /**
   * Tests the setters and getters.
   */
  public function testEvent() {
    $ignoredFieldTypes = ['password', 'hidden'];
    $verifiableFieldTypes = ['string', 'textfield'];

    $event = new MosparoIntegrationFilterFieldTypesEvent($ignoredFieldTypes, $verifiableFieldTypes);
    $this->assertEquals($event->getIgnoredFieldTypes(), $ignoredFieldTypes);
    $this->assertEquals($event->getVerifiableFieldTypes(), $verifiableFieldTypes);

    $newIgnoredFieldTypes = array_merge($ignoredFieldTypes, ['button']);
    $newVerifiableFieldTypes = array_merge($verifiableFieldTypes, ['textarea']);

    $event->setIgnoredFieldTypes($newIgnoredFieldTypes);
    $event->setVerifiableFieldTypes($newVerifiableFieldTypes);

    $this->assertEquals($event->getIgnoredFieldTypes(), $newIgnoredFieldTypes);
    $this->assertEquals($event->getVerifiableFieldTypes(), $newVerifiableFieldTypes);
  }

}
