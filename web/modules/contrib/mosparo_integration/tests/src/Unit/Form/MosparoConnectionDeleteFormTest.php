<?php

namespace Drupal\Tests\mosparo_integration\Unit\Event;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Form\FormState;
use Drupal\Core\Messenger\Messenger;
use Drupal\Core\Url;
use Drupal\mosparo_integration\Entity\MosparoConnection;
use Drupal\mosparo_integration\Form\MosparoConnectionDeleteForm;
use Drupal\Tests\UnitTestCase;
use Prophecy\Argument;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Tests the mosparo connection delete form.
 *
 * @coversDefaultClass \Drupal\mosparo_integration\Form\MosparoConnectionDeleteForm
 *
 * @group mosparo_integration
 */
class MosparoConnectionDeleteFormTest extends UnitTestCase {

  /**
   * Tests the getters.
   *
   * @covers ::getQuestion
   * @covers ::getCancelUrl
   * @covers ::getConfirmText
   */
  public function testFormGetters() {
    $mockContainer = $this->prophesize(ContainerInterface::class);
    \Drupal::setContainer($mockContainer->reveal());

    $testEntity = $this->prophesize(MosparoConnection::class);
    $testEntity->label()->willReturn('testEntity1');

    $form = new MosparoConnectionDeleteForm();
    $form->setEntity($testEntity->reveal());

    $this->assertInstanceOf(FormattableMarkup::class, $form->getQuestion());
    $this->assertInstanceOf(Url::class, $form->getCancelUrl());
    $this->assertInstanceOf(FormattableMarkup::class, $form->getConfirmText());
  }

  /**
   * Tests the submitForm method.
   *
   * @covers ::submitForm
   */
  public function testSubmitForm() {
    $mockContainer = $this->prophesize(ContainerInterface::class);
    \Drupal::setContainer($mockContainer->reveal());

    $testEntity1 = $this->prophesize(MosparoConnection::class);

    $mockMessenger = $this->prophesize(Messenger::class);
    $mockMessenger->addMessage(Argument::type(FormattableMarkup::class));

    $formState = new FormState();

    $form = new MosparoConnectionDeleteForm();
    $form->setEntity($testEntity1->reveal());
    $form->setMessenger($mockMessenger->reveal());

    $formData = [];

    $form->submitForm($formData, $formState);

    $this->assertInstanceOf(Url::class, $formState->getRedirect());
  }

}
