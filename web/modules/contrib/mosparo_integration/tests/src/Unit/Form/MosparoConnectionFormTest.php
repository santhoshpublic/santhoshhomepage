<?php

namespace Drupal\Tests\mosparo_integration\Unit\Event;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Config\Entity\ConfigEntityStorage;
use Drupal\Core\Config\Entity\Query\Query;
use Drupal\Core\Entity\EntityType;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Form\FormState;
use Drupal\Core\Messenger\Messenger;
use Drupal\Core\StringTranslation\TranslationManager;
use Drupal\Core\Url;
use Drupal\mosparo_integration\Entity\MosparoConnection;
use Drupal\mosparo_integration\Form\MosparoConnectionForm;
use Drupal\Tests\UnitTestCase;
use Prophecy\Argument;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Tests the mosparo connection form.
 *
 * @coversDefaultClass \Drupal\mosparo_integration\Form\MosparoConnectionForm
 *
 * @group mosparo_integration
 */
class MosparoConnectionFormTest extends UnitTestCase {

  /**
   * A mock of the Container.
   *
   * @var \Symfony\Component\DependencyInjection\ContainerInterface
   */
  protected $mockContainer;

  /**
   * A mock of the EntityTypeManager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $mockEntityTypeManager;

  /**
   * A mock of the TranslationManager.
   *
   * @var \Drupal\Core\StringTranslation\TranslationManager
   */
  protected $mockStringTranslation;

  /**
   * A mock of the EntityType.
   *
   * @var \Drupal\Core\Entity\EntityType
   */
  protected $mockEntityType;

  /**
   * A mock of the EntityStorage.
   *
   * @var \Drupal\Core\Config\Entity\ConfigEntityStorage
   */
  protected $mockEntityStorage;

  /**
   * A test entity.
   *
   * @var \Drupal\mosparo_integration\Entity\MosparoConnection
   */
  protected $testEntity;

  /**
   * The form that this test is testing.
   *
   * @var \Drupal\mosparo_integration\Form\MosparoConnectionForm
   */
  protected $form;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $this->mockEntityType = $this->prophesize(EntityType::class);
    $this->mockEntityType->getKey('label')->willReturn('label');

    $this->mockEntityStorage = $this->prophesize(ConfigEntityStorage::class);

    $this->mockEntityTypeManager = $this->prophesize(EntityTypeManager::class);
    $this->mockEntityTypeManager->getDefinition('mosparo_connection')->willReturn($this->mockEntityType->reveal());
    $this->mockEntityTypeManager->getStorage('mosparo_connection')->willReturn($this->mockEntityStorage->reveal());

    $this->mockStringTranslation = $this->prophesize(TranslationManager::class);

    $this->mockContainer = $this->prophesize(ContainerInterface::class);
    $this->mockContainer->get('entity_type.manager')->willReturn($this->mockEntityTypeManager->reveal());
    \Drupal::setContainer($this->mockContainer->reveal());

    $this->testEntity = new MosparoConnection([
      'id' => 1,
      'label' => 'Test Entity',
      'mosparoHost' => 'https://mosparo.example.com',
      'mosparoUuid' => 'UUID',
      'mosparoPublicKey' => 'publicKey',
      'mosparoPrivateKey' => 'privateKey',
    ], 'mosparo_connection');
  }

  /**
   * Tests the generate form method.
   *
   * @covers ::create
   * @covers ::__construct
   * @covers ::form
   */
  public function testGetForm() {
    $form = MosparoConnectionForm::create($this->mockContainer->reveal());
    $form->setStringTranslation($this->mockStringTranslation->reveal());
    $form->setEntity($this->testEntity);

    $formData = [];
    $formState = new FormState();

    $formFields = $form->form($formData, $formState);

    $this->assertIsArray($formFields);
    $this->assertArrayHasKey('howToUse', $formFields);
    $this->assertArrayHasKey('label', $formFields);
    $this->assertArrayHasKey('id', $formFields);
    $this->assertArrayHasKey('mosparoHost', $formFields);
    $this->assertArrayHasKey('mosparoUuid', $formFields);
    $this->assertArrayHasKey('mosparoPublicKey', $formFields);
    $this->assertArrayHasKey('mosparoPrivateKey', $formFields);
    $this->assertArrayHasKey('verifySsl', $formFields);
  }

  /**
   * Tests the save method.
   *
   * @covers ::save
   */
  public function testSaveNewForm() {
    $this->mockEntityStorage->save(Argument::type(MosparoConnection::class))->willReturn(SAVED_NEW);

    $mockMessenger = $this->prophesize(Messenger::class);
    $mockMessenger->addMessage(Argument::type(FormattableMarkup::class));

    $form = MosparoConnectionForm::create($this->mockContainer->reveal());
    $form->setStringTranslation($this->mockStringTranslation->reveal());
    $form->setMessenger($mockMessenger->reveal());
    $form->setEntity($this->testEntity);

    $formData = [];
    $formState = new FormState();

    $form->save($formData, $formState);

    $this->assertInstanceOf(Url::class, $formState->getRedirect());
  }

  /**
   * Tests the save method.
   *
   * @covers ::save
   */
  public function testSaveExistingForm() {
    $this->mockEntityStorage->save(Argument::type(MosparoConnection::class))->willReturn(SAVED_UPDATED);

    $mockMessenger = $this->prophesize(Messenger::class);
    $mockMessenger->addMessage(Argument::type(FormattableMarkup::class));

    $form = MosparoConnectionForm::create($this->mockContainer->reveal());
    $form->setStringTranslation($this->mockStringTranslation->reveal());
    $form->setMessenger($mockMessenger->reveal());
    $form->setEntity($this->testEntity);

    $formData = [];
    $formState = new FormState();

    $form->save($formData, $formState);

    $this->assertInstanceOf(Url::class, $formState->getRedirect());
  }

  /**
   * Tests the exist method.
   *
   * @covers ::exist
   */
  public function testExist() {
    $entity = new MosparoConnection([], 'mosparo_connection');

    $mockQuery = $this->prophesize(Query::class);
    $mockQuery->accessCheck(TRUE)->willReturn($mockQuery->reveal());
    $mockQuery->condition('id', 1)->willReturn($mockQuery->reveal());
    $mockQuery->execute()->willReturn($entity);

    $this->mockEntityStorage->getQuery()->willReturn($mockQuery->reveal());

    $form = MosparoConnectionForm::create($this->mockContainer->reveal());

    $this->assertTrue($form->exist(1));
  }

}

if (!defined('SAVED_NEW')) {
  define('SAVED_NEW', 1);
}

if (!defined('SAVED_UPDATED')) {
  define('SAVED_UPDATED', 2);
}
