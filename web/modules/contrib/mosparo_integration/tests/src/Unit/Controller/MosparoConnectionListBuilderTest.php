<?php

namespace Drupal\Tests\mosparo_integration\Unit\Controller;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Config\Entity\ConfigEntityStorage;
use Drupal\Core\Entity\EntityType;
use Drupal\Core\Entity\Query\QueryBase;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\mosparo_integration\Controller\MosparoConnectionListBuilder;
use Drupal\mosparo_integration\Entity\MosparoConnection;
use Drupal\Tests\UnitTestCase;
use Prophecy\Argument;
use Prophecy\PhpUnit\ProphecyTrait;
use Prophecy\Prophecy\ObjectProphecy;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Tests the list builder for the mosparo connection list.
 *
 * @coversDefaultClass \Drupal\mosparo_integration\Controller\MosparoConnectionListBuilder
 *
 * @group mosparo_integration
 */
class MosparoConnectionListBuilderTest extends UnitTestCase {

  use ProphecyTrait;

  /**
   * A mock of the ModuleHandler.
   *
   * @var \Prophecy\Prophecy\ObjectProphecy
   */
  protected ObjectProphecy $mockModuleHandler;

  /**
   * A mock of the Container.
   *
   * @var \Prophecy\Prophecy\ObjectProphecy
   */
  protected ObjectProphecy $mockContainer;

  /**
   * A mock of the EntityType.
   *
   * @var \Prophecy\Prophecy\ObjectProphecy
   */
  protected ObjectProphecy $mockEntityType;

  /**
   * A mock of the EntityStorage.
   *
   * @var \Prophecy\Prophecy\ObjectProphecy
   */
  protected ObjectProphecy $mockEntityStorage;

  /**
   * The list builder.
   *
   * @var \Drupal\mosparo_integration\Controller\MosparoConnectionListBuilder
   */
  protected MosparoConnectionListBuilder $listBuilder;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $this->mockModuleHandler = $this->prophesize(ModuleHandlerInterface::class);
    $this->mockModuleHandler->invokeAll(Argument::any(), Argument::any())->willReturn([]);
    $this->mockModuleHandler->alter(Argument::any(), Argument::any(), Argument::any())->willReturn([]);

    $this->mockContainer = $this->prophesize(ContainerInterface::class);
    $this->mockContainer->get('string_translation')->willReturn($this->getStringTranslationStub());
    $this->mockContainer->get('module_handler')->willReturn($this->mockModuleHandler->reveal());

    $this->mockEntityType = $this->prophesize(EntityType::class);
    $this->mockEntityStorage = $this->prophesize(ConfigEntityStorage::class);
    $this->listBuilder = new MosparoConnectionListBuilder($this->mockEntityType->reveal(), $this->mockEntityStorage->reveal());

    \Drupal::setContainer($this->mockContainer->reveal());
  }

  /**
   * Tests the method to build the list header.
   *
   * @covers ::buildHeader
   */
  public function testBuildHeader() {
    $header = $this->listBuilder->buildHeader();
    $this->assertArrayHasKey('label', $header);
    $this->assertArrayHasKey('mosparoHost', $header);
  }

  /**
   * Tests the method to build the list row.
   *
   * @covers ::buildRow
   */
  public function testBuildRow() {
    $mockEntity = $this->prophesize(MosparoConnection::class);
    $mockEntity->access(Argument::any())->willReturn(FALSE);
    $mockEntity->id()->willReturn('test_connection');
    $mockEntity->getLabel()->willReturn('test Connection');
    $mockEntity->getMosparoHost()->willReturn('https://mosparo.local');
    $mockEntity->hasLinkTemplate('edit-form')->willReturn(FALSE);
    $mockEntity->hasLinkTemplate('delete-form')->willReturn(FALSE);

    $row = $this->listBuilder->buildRow($mockEntity->reveal());

    $this->assertArrayHasKey('label', $row);
    $this->assertEquals('test Connection', $row['label']);

    $this->assertArrayHasKey('mosparoHost', $row);
    $this->assertEquals('https://mosparo.local', $row['mosparoHost']);
  }

  /**
   * Tests the method to render the list.
   *
   * @covers ::render
   */
  public function testRender() {
    $mockQuery = $this->prophesize(QueryBase::class);
    $mockQuery->accessCheck(TRUE)->willReturn($mockQuery->reveal());
    $mockQuery->sort(Argument::any())->willReturn($mockQuery->reveal());
    $mockQuery->pager(Argument::any())->willReturn($mockQuery->reveal());
    $mockQuery->execute()->willReturn([1, 2]);

    $this->mockEntityType->getClass()->willReturn(ConfigEntityBase::class);
    $this->mockEntityType->getListCacheContexts()->willReturn([]);
    $this->mockEntityType->getListCacheTags()->willReturn([]);
    $this->mockEntityType->getKey('id')->willReturn('id');
    $this->mockEntityType->hasKey('status')->willReturn(FALSE);

    $testEntity1 = $this->prophesize(MosparoConnection::class);
    $testEntity1->id()->willReturn(1);
    $testEntity1->getLabel()->willReturn('testEntity1');
    $testEntity1->label()->willReturn('testEntity1');
    $testEntity1->getMosparoHost()->willReturn('https://mosparo.example.com');
    $testEntity1->access('update')->willReturn(FALSE);
    $testEntity1->access('delete')->willReturn(FALSE);
    $testEntity2 = $this->prophesize(MosparoConnection::class);
    $testEntity2->id()->willReturn(2);
    $testEntity2->getLabel()->willReturn('testEntity2');
    $testEntity2->label()->willReturn('testEntity2');
    $testEntity2->getMosparoHost()->willReturn('https://mosparo.example.com');
    $testEntity2->access('update')->willReturn(FALSE);
    $testEntity2->access('delete')->willReturn(FALSE);

    $this->mockEntityStorage->getQuery()->willReturn($mockQuery->reveal());
    $this->mockEntityStorage->loadMultipleOverrideFree([1, 2])->willReturn([
      $testEntity1->reveal(),
      $testEntity2->reveal(),
    ]);

    $build = $this->listBuilder->render();

    $this->assertIsArray($build);
    $this->assertArrayHasKey('table', $build);

    $this->assertArrayHasKey('#empty', $build['table']);
    $this->assertInstanceOf(FormattableMarkup::class, $build['table']['#empty']);

    $this->assertArrayHasKey('#header', $build['table']);
    $this->assertCount(3, $build['table']['#header']);

    $this->assertArrayHasKey('#rows', $build['table']);
    $this->assertCount(2, $build['table']['#rows']);
  }

}
