<?php

namespace Drupal\outlayer;

/**
 * Defines re-usable methods for outlayer hook_alter.
 */
interface OutlayerHookInterface {

  /**
   * Returns common button markup.
   *
   * @todo use a themeable output.
   */
  public static function button(array $variables): array;

  /**
   * Implements hook_config_schema_info_alter().
   */
  public function configSchemaInfoAlter(array &$definitions): void;

  /**
   * Implements hook_library_info_build().
   */
  public function libraryInfoBuild(): array;

  /**
   * Implements hook_library_info_alter().
   */
  public function libraryInfoAlter(array &$libraries, $extension): void;

}
