<?php

namespace Drupal\outlayer\Plugin\views\style;

use Drupal\gridstack\Plugin\views\style\GridStackViewsInterface;

/**
 * Provides an interface defining Outlayer Views.
 */
interface OutlayerViewsGridStackInterface extends GridStackViewsInterface {}
