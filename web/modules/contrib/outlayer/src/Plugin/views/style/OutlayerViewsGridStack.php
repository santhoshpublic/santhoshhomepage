<?php

namespace Drupal\outlayer\Plugin\views\style;

use Drupal\gridstack\Plugin\views\style\GridStackViews;
use Drupal\outlayer\OutlayerDefault;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Outlayer base style plugin for Isotope and Grid.
 */
class OutlayerViewsGridStack extends GridStackViews implements OutlayerViewsGridStackInterface {

  /**
   * {@inheritdoc}
   */
  protected static $namespace = 'outlayer';

  /**
   * {@inheritdoc}
   */
  protected static $itemId = 'box';

  /**
   * {@inheritdoc}
   */
  protected static $itemPrefix = 'box';

  /**
   * {@inheritdoc}
   */
  protected static $captionId = 'caption';

  /**
   * The outlayer service manager.
   *
   * @var \Drupal\outlayer\OutlayerManagerInterface
   */
  protected $manager;

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition
  ) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->manager = $container->get('outlayer.manager');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function admin() {
    return \Drupal::service('outlayer.admin');
  }

  /**
   * Provides commons settings for the style plugins.
   */
  protected function buildSettings() {
    $settings = parent::buildSettings();
    $settings += OutlayerDefault::htmlSettings();
    $blazies = $settings['blazies'];
    $outlayers = $settings['outlayers'];
    $instance = $blazies->get('view.name') . '-' . $blazies->get('view.view_mode');
    $instance = str_replace('_', '-', $instance);

    $outlayers->set('view.base_id', $instance);

    $blazies->set('item.id', static::$itemId)
      ->set('item.prefix', static::$itemPrefix)
      ->set('item.caption', static::$captionId)
      ->set('namespace', static::$namespace)
      ->set('is.noratio', TRUE);

    return $settings;
  }

  /**
   * {@inheritdoc}
   */
  protected function defineOptions() {
    $options = [];
    $settings = OutlayerDefault::gridSettings() + OutlayerDefault::viewsSettings();
    foreach ($settings as $key => $value) {
      $options[$key] = ['default' => $value];
    }
    return $options + parent::defineOptions();
  }

}
