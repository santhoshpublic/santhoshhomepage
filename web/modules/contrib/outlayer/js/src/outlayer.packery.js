/**
 * @file
 * Provides Outlayer Packery loader.
 */

(function ($, Drupal, Packery) {

  'use strict';

  var _id = 'outlayer';
  var _plugin = 'packery';
  var _idOnce = _id + '-' + _plugin;
  var _mounted = 'is-' + _plugin + '-on';
  var _selector = '.' + _id + '--' + _plugin + ':not(.' + _mounted + ')';

  /**
   * Outlayer utility functions.
   *
   * @namespace
   */
  $.outLayer.packery = $.extend($.outLayer || {}, {

    onClass: _plugin,

    load: function (el) {
      var me = this;

      me.$el = el;
      me.options = $.parse($.attr(el, 'data-' + _idOnce));

      // Prepare, define aspect ratio before laying out, and initialize.
      me.prepare(el);

      me.$instance = new Packery(el, me.options);

      // Runs update(), including on resize event, only if nobody helps.
      if (me.isUnGridStack) {
        me.buildOut(el);
      }

      me.cleanUp(el);
    }

  });

  /**
   * Attaches Outlayer behavior to HTML element.
   *
   * @type {Drupal~behavior}
   */
  Drupal.behaviors.outLayerPackery = {
    attach: function (context) {
      var me = $.outLayer.packery;
      $.once(me.init.bind(me), _idOnce, _selector, context);
    },
    detach: function (context, settings, trigger) {
      if (trigger === 'unload') {
        $.once.removeSafely(_idOnce, _selector, context);
      }
    }
  };

}(dBlazy, Drupal, Packery));
