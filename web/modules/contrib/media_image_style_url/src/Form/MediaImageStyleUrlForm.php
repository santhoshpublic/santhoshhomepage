<?php

namespace Drupal\media_image_style_url\Form;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\StreamWrapper\StreamWrapperManagerInterface;
use Drupal\Core\Url;
use Drupal\file\FileInterface;
use Drupal\media\Plugin\media\Source\Image;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Generate the media image style URL.
 */
class MediaImageStyleUrlForm extends FormBase {

  /**
   * The route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected $routeMatch;

  /**
   * The file storaeg.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $fileStorage;

  /**
   * The image style storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  protected $imageStyleStorage;

  /**
   * The stream wrapper manager.
   *
   * @var \Drupal\Core\StreamWrapper\StreamWrapperManagerInterface
   */
  protected $streamWrapperManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(RouteMatchInterface $route_match, EntityTypeManagerInterface $entity_type_manager, StreamWrapperManagerInterface $stream_wrapper_manager) {
    $this->routeMatch = $route_match;
    $this->fileStorage = $entity_type_manager->getStorage('file');
    $this->imageStyleStorage = $entity_type_manager->getStorage('image_style');
    $this->streamWrapperManager = $stream_wrapper_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('current_route_match'),
      $container->get('entity_type.manager'),
      $container->get('stream_wrapper_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'media_image_style_url_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['image_style'] = [
      '#type' => 'select',
      '#title' => $this->t('Image style'),
      '#options' => image_style_options(TRUE),
      '#required' => TRUE,
    ];

    $form['actions'] = ['#type' => 'actions'];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#button_type' => 'primary',
      '#value' => $this->t('Generate'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\media\MediaInterface $media */
    $media = $this->routeMatch->getParameter('media');

    $source = $media->getSource();
    $config = $source->getConfiguration();
    $field = $config['source_field'];

    if (!$source instanceof Image) {
      $this->messenger()->addWarning('Only images support image styles.');
      return;
    }

    $fid = $media->{$field}->target_id;

    // If media has no file item.
    if (!$fid) {
      $this->messenger()->addWarning('The media item requested has no file referenced/uploaded in the %field field.', ['%field' => $field]);
      return;
    }

    $file = $this->fileStorage->load($fid);

    // If media has no file item.
    if (!$file instanceof FileInterface) {
      $this->messenger()->addWarning('The media item requested has no file referenced/uploaded in the %field field.', ['%field' => $field]);
      return;
    }

    $uri = $file->getFileUri();
    $scheme = $this->streamWrapperManager->getScheme($uri);

    // Or item does not exist on disk.
    if (!$this->streamWrapperManager->isValidScheme($scheme) || !file_exists($uri)) {
      $this->messenger()->addWarning($this->t('The file %uri does not exist.', ['%uri' => $uri]));
      return;
    }

    // Disable the destination redirect.
    $this->getRequest()->query->set('destination', NULL);
    $image_style = $this->imageStyleStorage->load($form_state->getValue('image_style'));
    $form_state->setRedirectUrl(Url::fromUri($image_style->buildUrl($uri)));
  }

}
