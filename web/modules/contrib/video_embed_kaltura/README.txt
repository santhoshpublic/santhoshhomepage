CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

Kaltura Video Embed Field is a submodule of Video Embed Field for Kaltura Video
support.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/video_embed_kaltura

 * To submit bug reports and feature suggestions, or track changes:
   https://www.drupal.org/project/issues/video_embed_kaltura


REQUIREMENTS
------------

 * Video Embed Field (https://www.drupal.org/project/video_embed_field):
   Video Embed field creates a simple field type that allows you to embed videos
   from YouTube and Vimeo and show their thumbnail previews simply by entering
   the video's url.


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module.
   See: https://www.drupal.org/node/895232 for further information.


CONFIGURATION
-------------

 * Navigate to '/admin/config/media/kaltura-config':
   - Enter the Partner Id, Uiconf Id details required by the module to use.

 * kaltura settings (Change Kaltura video settings):
   Permission to change or modify the module configuration.


MAINTAINERS
-----------

Current maintainers:
 * Abhinand Gokhala K - https://www.drupal.org/u/abhinand-gokhala-k
