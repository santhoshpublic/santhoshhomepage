<?php

namespace Drupal\ajaxin\Form;

use Drupal\ajaxin\AjaxinInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines Ajaxin admin settings form.
 */
class AjaxinSettingsForm extends ConfigFormBase {

  /**
   * The ajaxin service.
   *
   * @var \Drupal\ajaxin\AjaxinInterface
   */
  protected $ajaxin;

  /**
   * Constructs a \Drupal\system\ConfigFormBase object.
   */
  public function __construct(ConfigFactoryInterface $config_factory, AjaxinInterface $ajaxin) {
    parent::__construct($config_factory);

    $this->ajaxin = $ajaxin;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('ajaxin')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'ajaxin_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['ajaxin.settings'];
  }

  /**
   * Implements \Drupal\Core\Form\FormInterface::buildForm().
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('ajaxin.settings');

    $form['sitewide'] = [
      '#type'          => 'checkbox',
      '#title'         => $this->t('Enable sitewide'),
      '#default_value' => $config->get('sitewide'),
      '#description'   => $this->t("By default only enabled when Blazy or Drupal Ajax is available on the page. Check to always be available sitewide in case you need the provided loading animation for custom works. Leave it unchecked for no custom works. It won't display anything till you initialize it. Check out ajaxin.blazy.js for samples."),
    ];

    $form['color'] = [
      '#type'          => 'select',
      '#title'         => $this->t('Background color'),
      '#options'       => [
        'blue'   => $this->t('Blue'),
        'lime'   => $this->t('Lime'),
        'orange' => $this->t('Orange'),
        'purple' => $this->t('Purple'),
        'red'    => $this->t('Red'),
      ],
      '#default_value' => $config->get('color'),
      '#description'   => $this->t('Choose a different background color, default to Orange.'),
    ];

    $form['skin'] = [
      '#type'          => 'radios',
      '#title'         => $this->t('Skin'),
      '#options'       => $this->getSkinOptions(),
      '#default_value' => $config->get('skin'),
      '#description'   => $this->t('Choose the loading animation.'),
      '#attributes'    => ['class' => ['form-wrapper--ajaxin']],
      '#required'      => TRUE,
      '#suffix'        => '<h4>' . $this->t('Tips: Clear cache if not immediately updated.') . '</h4>',
    ];

    foreach ($this->ajaxin->getSkins() as $skin => $number) {
      $render = [
        '#theme' => 'ajaxin',
        '#skin' => $skin,
        '#color' => $config->get('color'),
      ];
      $form['skin'][$skin]['#field_prefix'] = $this->ajaxin->renderer()->render($render);
    }

    $form['#attached']['library'][] = 'ajaxin/admin';

    return parent::buildForm($form, $form_state);
  }

  /**
   * Returns available skins.
   */
  public function getSkinOptions() {
    $skins = [];
    foreach ($this->ajaxin->getSkins() as $skin => $number) {
      $skins[$skin] = $skin;
    }
    return $skins;
  }

  /**
   * Implements \Drupal\Core\Form\FormInterface::submitForm().
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->configFactory->getEditable('ajaxin.settings');

    $config->set('sitewide', $form_state->getValue('sitewide'));
    $config->set('color', $form_state->getValue('color'));
    $config->set('skin', $form_state->getValue('skin'));
    $config->save();

    $this->configFactory->clearStaticCache();

    parent::submitForm($form, $form_state);
  }

}
