# Twig "real_content" empty region / content check helper

*Provides a "real_content" twig filter and test to determine if a certain twig variable (typically a region) has meaningful content is empty. Read the super long core issue for details.*

See https://www.drupal.org/project/twig_real_content for details.
