<?php

namespace Drupal\twig_real_content\Exceptions;

/**
 * Helper class for specific TwigRealContentException.
 */
class TwigRealContentException extends \Exception {

}
