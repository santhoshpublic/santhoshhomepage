<?php

namespace Drupal\drowl_layouts\Plugin\Layout;

use Drupal\Core\Form\FormStateInterface;

/**
 * Layout plugin class to provide general options for MultiWidthLayout.
 *
 * @internal
 *   Plugin classes are internal.
 */
trait DrowlLayoutsSettingsTrait {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return parent::defaultConfiguration() + [
      // Set OUR defaults:
      'layout_section_width' => 'viewport-width-cp',
      'layout_align_cells_vertical' => 'stretch',
      'layout_align_cells_horizontal' => 'left',
      'layout_remove_grid_gutter' => '',
      'extra_classes' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $configuration = $this->getConfiguration();
    // Add custom configuration:
    // -- Section width --.
    $field_name = 'layout_section_width';
    $wrapper_name = $field_name . '_wrapper';
    $form[$wrapper_name] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => ['form-wrapper--drowl-layouts-settings-preview'],
      ],
    ];
    $form[$wrapper_name][$field_name . '_preview'] = [
      '#theme' => 'drowl_layouts_settings_preview_section_width',
      '#field_related' => $field_name,
    ];
    $form[$wrapper_name][$field_name] = [
      '#type' => 'select',
      '#title' => $this->t('Section width'),
      '#options' => [
        'page-width' => $this->t('Page width (all)'),
        'viewport-width' => $this->t('Viewport width (all)'),
        'viewport-width-cp' => $this->t('Viewport width (only background)'),
      ],
      '#default_value' => $configuration[$field_name],
      '#empty_option' => $this->t('- None -'),
      '#required' => FALSE,
      '#description' => $this->t('Overrides the container width, ignoring the parent container width. Viewport width = screen width, Page width = content width.'),
      '#wrapper_attributes' => ['class' => ['form-item--layout-section-width']],
    ];

    // -- Children vertical alignment --
    $field_name = 'layout_align_cells_vertical';
    $wrapper_name = $field_name . '_wrapper';
    $form[$wrapper_name] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => ['form-wrapper--drowl-layouts-settings-preview'],
      ],
    ];
    $form[$wrapper_name][$field_name . '_preview'] = [
      '#theme' => 'drowl_layouts_settings_preview_cell_alignment',
      '#field_related' => $field_name,
    ];
    $form[$wrapper_name][$field_name] = [
      '#type' => 'select',
      '#title' => $this->t('Children vertical alignment'),
      '#options' => [
        'top' => $this->t('Top'),
        'middle' => $this->t('Middle'),
        'bottom' => $this->t('Bottom'),
        'stretch' => $this->t('Stretch'),
      ],
      '#default_value' => $configuration[$field_name],
      '#empty_option' => $this->t('- None -'),
      '#required' => FALSE,
      '#description' => $this->t('Aligns the children elements vertically. In some cases this setting may have no effect.'),
      '#wrapper_attributes' => ['class' => ['form-item--layout-align-cells-vertical']],
    ];

    // -- Children horizontal alignment --
    $field_name = 'layout_align_cells_horizontal';
    $wrapper_name = $field_name . '_wrapper';
    $form[$wrapper_name] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => ['form-wrapper--drowl-layouts-settings-preview'],
      ],
    ];
    $form[$wrapper_name][$field_name . '_preview'] = [
      '#theme' => 'drowl_layouts_settings_preview_cell_alignment',
      '#field_related' => $field_name,
    ];
    $form[$wrapper_name][$field_name] = [
      '#type' => 'select',
      '#title' => $this->t('Children horizontal alignment'),
      '#options' => [
        'left' => $this->t('Left'),
        'center' => $this->t('Center'),
        'right' => $this->t('Right'),
        'justify' => $this->t('Aligned to the edges (justify)'),
        'spaced' => $this->t('Aligned to the space around (spaced)'),
      ],
      '#default_value' => $configuration[$field_name],
      '#empty_option' => $this->t('- None -'),
      '#required' => FALSE,
      '#description' => $this->t('Aligns the children elements horizontally. In some cases this setting may have no effect.'),
      '#wrapper_attributes' => ['class' => ['form-item--layout-align-children-horizontal']],
    ];

    // -- Remove grid spaces --
    $field_name = 'layout_remove_grid_gutter';
    $wrapper_name = $field_name . '_wrapper';
    $form[$wrapper_name] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => ['form-wrapper--drowl-layouts-settings-preview'],
      ],
    ];
    $form[$wrapper_name][$field_name . '_preview'] = [
      '#theme' => 'drowl_layouts_settings_preview_grid_gutter',
      '#field_related' => $field_name,
    ];
    $form[$wrapper_name][$field_name] = [
      '#type' => 'select',
      '#title' => $this->t('Remove grid spaces'),
      '#options' => [
        'large' => $this->t('Large'),
        'medium' => $this->t('Medium'),
        'small' => $this->t('Small'),
      ],
      '#default_value' => $configuration[$field_name],
      '#empty_option' => $this->t('- None -'),
      '#required' => FALSE,
      '#multiple' => TRUE,
      '#description' => $this->t('Remove the spaces between the layout columns (by device size).'),
      '#wrapper_attributes' => ['class' => ['form-item--layout-remove-grid-gutter']],
    ];

    $form['extra_classes'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Extra classes'),
      '#default_value' => $configuration['extra_classes'],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    // @todo We should better massage the form values here, but neither #tree = true nor other things work... So we use the nested array so far.
    $this->configuration['layout_section_width'] = $form_state->getValue('layout_section_width_wrapper')['layout_section_width'];
    $this->configuration['layout_align_cells_vertical'] = $form_state->getValue('layout_align_cells_vertical_wrapper')['layout_align_cells_vertical'];
    $this->configuration['layout_align_cells_horizontal'] = $form_state->getValue('layout_align_cells_horizontal_wrapper')['layout_align_cells_horizontal'];
    $this->configuration['layout_remove_grid_gutter'] = $form_state->getValue('layout_remove_grid_gutter_wrapper')['layout_remove_grid_gutter'];
    $this->configuration['extra_classes'] = $form_state->getValue('extra_classes');
  }

}
