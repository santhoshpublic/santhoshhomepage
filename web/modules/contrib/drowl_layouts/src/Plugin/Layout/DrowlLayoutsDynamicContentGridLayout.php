<?php

namespace Drupal\drowl_layouts\Plugin\Layout;

use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configurable dynamic content column layout plugin class.
 *
 * Dynamic content grid layout has "one column" and its contents are aligned
 * by css grid.
 * For details see: https://www.drupal.org/project/drowl_layouts/issues/3307235
 *
 * @internal
 *   Plugin classes are internal.
 */
class DrowlLayoutsDynamicContentGridLayout extends DrowlLayoutsLayoutDefault implements PluginFormInterface {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return parent::defaultConfiguration() + [
      // Add dynamic Layout settings:
      'layout_dynamic_grid_col_min_width' => 10,
      'layout_dynamic_grid_col_max_width' => 10,
      'layout_dynamic_grid_gutter_size' => 'md',
      'layout_dynamic_grid_behavoir' => 'auto-fit',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    // Add specific options for the dynamic content grid:
    $grid_gutter_widths = [
      // Sizing options for danymic grid columns/cells:
      'none' => $this->t('No Gutter'),
      'xxs' => 'XXS',
      'xs' => 'XS',
      'sm' => 'SM',
      'md' => 'MD',
      'lg' => 'LG',
      'xl' => 'XL',
      'xxl' => 'XXL',
    ];

    // Column Size by min/max values:
    $form['layout_dynamic_grid_col_sizing_wrapper'] = [
      '#type' => 'container',
      '#attributes' => ['class' => ['dynamic-grid-wrapper']],
    ];
    $form['layout_dynamic_grid_col_sizing_wrapper']['layout_dynamic_grid_col_min_width'] = [
      '#title' => $this->t('Minimum Width:'),
      '#type' => 'textfield',
      '#default_value' => $this->configuration['layout_dynamic_grid_col_min_width'],
      '#wrapper_attributes' => ['class' => ['form-item--dynamic-grid-col-min-width']],
      '#description' => $this->t('Minimum width as pixel (px) value'),
      '#size' => 32,
      '#maxlength' => 128,
      '#required' => FALSE,
      '#field_suffix' => 'px',
    ];
    $form['layout_dynamic_grid_col_sizing_wrapper']['layout_dynamic_grid_col_max_width'] = [
      '#title' => $this->t('Maximum Width:'),
      '#type' => 'textfield',
      '#default_value' => $this->configuration['layout_dynamic_grid_col_max_width'],
      '#wrapper_attributes' => ['class' => ['form-item--dynamic-grid-col-max-width']],
      '#description' => $this->t('Maximum width as pixel (px) value'),
      '#size' => 32,
      '#maxlength' => 128,
      '#required' => FALSE,
      '#field_suffix' => 'px',
    ];

    // Grid Gutter Size:
    $form['layout_dynamic_grid_gutter_wrapper'] = [
      '#type' => 'container',
      '#attributes' => ['class' => ['dynamic-grid-gutter-wrapper']],
    ];
    $form['layout_dynamic_grid_gutter_wrapper']['layout_dynamic_grid_gutter_size'] = [
      '#type' => 'select',
      '#title' => $this->t('Grid Gutter size:'),
      '#options' => $grid_gutter_widths,
      '#default_value' => $this->configuration['layout_dynamic_grid_gutter_size'],
      '#wrapper_attributes' => ['class' => ['form-item--dynamic-grid-gutter-size']],
      '#empty_option' => $this->t('- Default (from Theme Grid System) -'),
      '#required' => FALSE,
    ];
    $form['layout_dynamic_grid_gutter_wrapper']['layout_dynamic_grid_behavoir'] = [
      '#type' => 'radios',
      '#title' => $this->t('Grid behavoir:'),
      '#options' => [
        'auto-fit' => $this->t('Use same column width, even if the last row has fewer content/columns (default)'),
        'auto-fill' => $this->t('Fill available space (last rows content/columns might grow in width to fill all available space)'),
      ],
      '#default_value' => $this->configuration['layout_dynamic_grid_behavoir'],
      '#wrapper_attributes' => ['class' => ['form-item--dynamic-grid-behavoir']],
      // '#empty_option' => $this->t('- Default (from Theme Grid System) -'),
      '#required' => FALSE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    // @todo We should better massage the form values here, but neither #tree = true nor other things work... So we use the nested array so far.
    // Add dynamic layout settings:
    $this->configuration['layout_dynamic_grid_col_min_width'] = $form_state->getValue('layout_dynamic_grid_col_sizing_wrapper')['layout_dynamic_grid_col_min_width'];
    $this->configuration['layout_dynamic_grid_col_max_width'] = $form_state->getValue('layout_dynamic_grid_col_sizing_wrapper')['layout_dynamic_grid_col_max_width'];
    $this->configuration['layout_dynamic_grid_behavoir'] = $form_state->getValue('layout_dynamic_grid_gutter_wrapper')['layout_dynamic_grid_behavoir'];
    $this->configuration['layout_dynamic_grid_gutter_size'] = $form_state->getValue('layout_dynamic_grid_gutter_wrapper')['layout_dynamic_grid_gutter_size'];
  }

}
