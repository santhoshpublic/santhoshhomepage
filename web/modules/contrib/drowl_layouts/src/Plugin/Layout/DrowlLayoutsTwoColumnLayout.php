<?php

namespace Drupal\drowl_layouts\Plugin\Layout;

use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configurable two column layout plugin class.
 *
 * @internal
 *   Plugin classes are internal.
 */
class DrowlLayoutsTwoColumnLayout extends DrowlLayoutsMultiWidthLayoutBase implements PluginFormInterface {

  /**
   * Returns the number of columns in this layout.
   *
   * @var int
   */
  protected $columnCount = 2;

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $field_name = 'layout_section_width';
    $wrapper_name = $field_name . '_wrapper';
    $form = parent::buildConfigurationForm($form, $form_state);
    // Add specific two column options:
    $form[$wrapper_name][$field_name]['#options']['viewport-width-column-first'] = $this->t('Viewport width (only first column)');
    $form[$wrapper_name][$field_name]['#options']['viewport-width-column-last'] = $this->t('Viewport width (only last column)');

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getColumnCount() {
    return $this->columnCount;
  }

  /**
   * {@inheritdoc}
   */
  protected function getWidthOptions() {
    return [
      '50-50' => '50%/50%',
      '66-33' => '66%/33%',
      '33-66' => '33%/66%',
    ];
  }

}
