<?php

namespace Drupal\ntc\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Checks that the node.
 *
 * @Constraint(
 *   id = "Ntc",
 *   label = @Translation("List of items"),
 * )
 */
class NodeTitleConstraint extends Constraint {

}
