INTRODUCTION
------------
A light weight module, helps to check node titles with
min/max number of character, unwanted characters/words
and maintains unique node title. Help you better SEO for Title. 

Check the node title By:

 * Special Character & unwanted words.
 * Length (optionally specify min and/or max length.
 * Unique node title (for specific content type.


INSTALLATION
------------
 * Install contributed Drupal module as we do normally. See:
   https://drupal.org/documentation/install/modules-themes/modules-7
   for further information.


CONFIGURATION
-------------
 * Configure user permissions in Administration » People » Permissions:
    Node Title Check admin control

 * Configure Node Title Check in Administration » Configuration »
    Content authoring » Node Title Check

    admin/config/content/ntc


MAINTAINERS
-----------
Current maintainers:
 * Wendy William - https://www.drupal.org/u/drupalnesia
