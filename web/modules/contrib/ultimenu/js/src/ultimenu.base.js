/**
 * @file
 * Provides mobile toggler for the the Ultimenu main block.
 */

(function (Drupal, drupalSettings, _win, _doc) {

  'use strict';

  var NAME = 'ultimenu';
  var C_IS_NAME = 'is-' + NAME;
  var C_CANVAS = C_IS_NAME + '-canvas';
  var IS_BODY_ACTIVE = C_CANVAS + '--active';
  var IS_BODY_HIDING = C_CANVAS + '--hiding';
  var IS_BODY_EXPANDED = C_IS_NAME + '-expanded';
  var IS_ITEM_EXPANDED = C_IS_NAME + '-item-expanded';
  var IS_LINK_ACTIVE = C_IS_NAME + '-active';
  var IS_HAMBURGER_ACTIVE = C_IS_NAME + '-button-active';
  var IS_AJAX_HIT = 'data-ultiajax-hit';
  var C_HIDDEN = 'hidden';
  var C_FLYOUT = 'flyout';
  var IS_FLYOUT = 'is-' + C_FLYOUT;
  var IS_FLYOUT_EXPANDED = IS_FLYOUT + '-expanded';
  var C_LINK = NAME + '__link';
  var S_LINK = '.' + C_LINK;
  var S_AJAX_LINK = '.' + NAME + '__ajax';
  var AJAX_TRIGGER = 'data-ultiajax-trigger';
  var C_ULTIMENU_FLYOUT = NAME + '__' + C_FLYOUT;
  var C_OFFCANVAS = C_CANVAS + '-off';
  var C_ONCANVAS = C_CANVAS + '-on';
  var C_BACKDROP = C_CANVAS + '-backdrop';
  var S_HAMBURGER = '[data-' + NAME + '-button]';
  var $BODY = _doc.body;
  var TIMER_HIDING = void 0;
  var TIMER_WAITING = void 0;
  var ADDCLASS = 'addClass';
  var REMOVECLASS = 'removeClass';
  var SETTINGS = drupalSettings.ultimenu || {};

  Drupal.ultimenu = {
    documentWidth: 0,
    $backdrop: null,
    $hamburger: null,
    $offCanvas: null,

    // @todo use dblazy at 3+ to DRY.
    isBlazy: 'dBlazy' in _win,

    context: function (context, selector) {
      if (this.isBlazy) {
        return _win.dBlazy.context(context, selector);
      }

      // Weirdo: context may be null after Colorbox close.
      context = context || document;

      // jQuery may pass its object as non-expected context identified by length.
      context = 'length' in context ? context[0] : context;
      return context || document;
    },

    // @todo remove for core/once when min D9.2.
    once: function (fn) {
      var result;
      var ran = false;
      return function proxy() {
        if (ran) {
          return result;
        }
        ran = true;
        result = fn.apply(this, arguments);
        // For garbage collection.
        fn = null;
        return result;
      };
    },

    each: function (collection, callback, scope) {
      if (this.isBlazy) {
        return _win.dBlazy.each(collection, callback, scope);
      }
      return collection.forEach(callback);
    },

    closest: function (el, selector) {
      if (this.isBlazy) {
        return _win.dBlazy.closest(el, selector);
      }
      return el.closest(selector);
    },

    find: function (ctx, selector) {
      return ctx.querySelector(selector);
    },

    findAll: function (ctx, selector) {
      return ctx.querySelectorAll(selector);
    },

    addRemoveClass: function (op, els, className) {
      var me = this;
      var classes = className.split(' ');
      if (els) {
        if (els.length) {
          me.each(els, function (el) {
            el.classList[op].apply(el.classList, classes);
          });
        }
        else if (els.classList) {
          els.classList[op].apply(els.classList, classes);
        }
      }
    },

    addClass: function (els, className) {
      this.addRemoveClass('add', els, className);
    },

    removeClass: function (els, className) {
      this.addRemoveClass('remove', els, className);
    },

    toggleClass: function (el, className) {
      if (el) {
        el.classList.toggle(className);
      }
    },

    slideToggle: function (el, className) {
      if (el) {
        this[el.clientHeight === 0 ? ADDCLASS : REMOVECLASS](el, className);
      }
    },

    doResizeMain: function () {
      var me = this;
      var width = _win.innerWidth || _doc.body.clientWidth;

      var closeOut = function () {
        me.removeClass($BODY, IS_BODY_EXPANDED + ' ' + IS_BODY_ACTIVE);
        me.closeFlyout();
      };

      // Do not cache the selector, to avoid incorrect classes with its cache.
      if (me.isHidden(me.find(_doc, S_HAMBURGER))) {
        closeOut();
      }
      else {
        me.addClass($BODY, IS_BODY_ACTIVE);
      }

      if (width !== me.documentWidth) {
        return;
      }

      if (me.isHidden(me.find(_doc, S_HAMBURGER))) {
        closeOut();
      }
      else if (width !== me.documentWidth) {
        me.addClass($BODY, IS_BODY_EXPANDED);

        if (!me.isHidden(me.find(_doc, S_HAMBURGER))) {
          me.addClass($BODY, IS_BODY_ACTIVE);
        }
      }

      me.documentWidth = width;
    },

    executeAjax: function (el) {
      var me = this;
      var $li = me.closest(el, 'li');
      var $ajax = me.find($li, S_AJAX_LINK);

      var cleanUp = function () {
        // Removes attribute to prevent this event from firing again.
        el.removeAttribute(AJAX_TRIGGER);
      };

      // The AJAX link will be gone on successful AJAX request.
      if ($ajax) {
        // Hover event can fire many times, prevents from too many clicks.
        if (!$ajax.hasAttribute(IS_AJAX_HIT)) {
          $ajax.click();

          $ajax.setAttribute(IS_AJAX_HIT, 1);
          me.addClass($ajax, C_HIDDEN);
        }

        // This is the last resort while the user is hovering over menu link.
        // If the AJAX link is still there, an error likely stops it, or
        // the AJAX is taking longer time than 1.5 seconds. In such a case,
        // TIMER_WAITING will re-fire the click event, yet on interval now.
        // At any rate, Drupal.Ajax.ajaxing manages the AJAX requests.
        _win.clearTimeout(TIMER_WAITING);
        TIMER_WAITING = _win.setTimeout(function () {
          $ajax = me.find($li, S_AJAX_LINK);
          if ($ajax) {
            me.removeClass($ajax, C_HIDDEN);
            $ajax.click();
          }
          else {
            cleanUp();
          }

          var onces = me.findAll($li, '[data-once]');
          if (onces.length) {
            me.each(onces, function (elOnce) {
              Drupal.attachBehaviors(elOnce);
            });
          }
        }, 1500);
      }
      else {
        cleanUp();
      }
    },

    triggerAjax: function (e) {
      var me = this;
      e.stopPropagation();

      var $link = e.target.classList.contains('caret') ? me.closest(e.target, S_LINK) : e.target;
      if ($link.hasAttribute(AJAX_TRIGGER)) {
        me.executeAjax($link);
      }
    },

    triggerClickHamburger: function (e) {
      var me = this;

      e.preventDefault();

      if (me.$hamburger) {
        me.$hamburger.click();
      }
    },

    doClickHamburger: function (e) {
      var me = this;

      e.preventDefault();
      e.stopPropagation();

      var $button = e.target;
      var expanded = $BODY.classList.contains(IS_BODY_EXPANDED);

      me[expanded ? REMOVECLASS : ADDCLASS]($BODY, IS_BODY_EXPANDED);
      me[expanded ? REMOVECLASS : ADDCLASS]($button, IS_HAMBURGER_ACTIVE);

      me.closeFlyout();

      // Cannot use transitionend as can be jumpy affected by child transitions.
      if (!expanded) {
        _win.clearTimeout(TIMER_HIDING);
        me.addClass($BODY, IS_BODY_HIDING);

        TIMER_HIDING = _win.setTimeout(function () {
          me.removeClass($BODY, IS_BODY_HIDING);
        }, 400);
      }

      // Scroll to top in case the current viewport is far below the fold.
      if (me.$backdrop) {
        _win.scroll({
          top: me.$backdrop.offsetTop,
          behavior: 'smooth'
        });
      }
    },

    closeFlyout: function () {
      var me = this;
      var actives = me.findAll(_doc, '.' + IS_LINK_ACTIVE);
      var expands = me.findAll(_doc, '.' + IS_ITEM_EXPANDED);
      var flyouts = me.findAll(_doc, '.' + IS_FLYOUT_EXPANDED);

      me.removeClass(actives, IS_LINK_ACTIVE);
      me.removeClass(expands, IS_ITEM_EXPANDED);
      me.removeClass(flyouts, IS_FLYOUT_EXPANDED);
    },

    isHidden: function (el) {
      if (el) {
        if (el.offsetParent === null) {
          return true;
        }
        var style = _win.getComputedStyle(el);
        return style.display === 'none' || style.visibility === 'invisible';
      }
      return false;
    },

    doClickCaret: function (e) {
      var me = this;

      e.preventDefault();
      e.stopPropagation();

      var $caret = e.target;
      var $link = me.closest($caret, S_LINK);
      var $li = me.closest($link, 'li');
      var $flyout = $link.nextElementSibling;

      // If hoverable for desktop, one at a time click should hide flyouts.
      // We let regular mobile toggle not affected, to avoid jumping accordion.
      if (me.isHidden(me.$hamburger)) {
        me.closeFlyout();
      }

      // Toggle the current flyout.
      if ($flyout && $flyout.classList.contains(C_ULTIMENU_FLYOUT)) {
        var hidden = $flyout.clientHeight === 0;

        me[hidden ? ADDCLASS : REMOVECLASS]($li, IS_ITEM_EXPANDED);
        me[hidden ? ADDCLASS : REMOVECLASS]($link, IS_LINK_ACTIVE);

        me.slideToggle($flyout, IS_FLYOUT_EXPANDED);
      }
    },

    onResize: function (c, t) {
      _win.onresize = function () {
        _win.clearTimeout(t);
        t = _win.setTimeout(c, 200);
      };
      return c;
    },

    prepare: function () {
      var me = this;
      var btnMain = '[data-ultimenu-button="#ultimenu-main"]';

      $BODY = _doc.body;
      me.$offCanvas = me.find(_doc, '.' + C_OFFCANVAS);
      me.$hamburger = me.find(_doc, S_HAMBURGER);
      me.$backdrop = me.find(_doc, '.' + C_BACKDROP);

      // Allows hard-coded CSS classes to not use this.
      if (SETTINGS.canvasOff && SETTINGS.canvasOn) {
        if (me.$offCanvas === null) {
          me.$offCanvas = me.find(_doc, SETTINGS.canvasOff);
          me.addClass(me.$offCanvas, C_OFFCANVAS);
        }

        var $onCanvas = me.find(_doc, '.' + C_ONCANVAS);
        if ($onCanvas === null) {
          var $onCanvases = me.findAll(_doc, SETTINGS.canvasOn);
          me.addClass($onCanvases, C_ONCANVAS);
        }
      }

      // Moves the hamburger button to the end of the body.
      if (me.find(_doc, 'body > ' + btnMain) === null) {
        var hamburger = me.find(_doc, btnMain);
        if (hamburger) {
          _doc.body.appendChild(hamburger);
        }
      }

      // Prepends our backdrop before the main off-canvas element.
      if (me.$backdrop === null && me.$offCanvas) {
        var $parent = me.$offCanvas.parentNode;
        var el = _doc.createElement('div');
        el.className = C_BACKDROP;
        $parent.insertBefore(el, $parent.firstElementChild || null);

        me.$backdrop = me.find(_doc, '.' + C_BACKDROP);
      }
    }

  };

})(Drupal, drupalSettings, this, this.document);
