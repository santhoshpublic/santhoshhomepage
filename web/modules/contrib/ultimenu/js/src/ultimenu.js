/**
 * @file
 * Provides mobile toggler for the the Ultimenu main block.
 */

(function ($, Drupal, drupalSettings) {

  'use strict';

  var NAME = 'ultimenu';
  var C_MOUNTED = NAME + '--on';
  var C_CANVAS = 'is-' + NAME + '-canvas';
  var C_BACKDROP = C_CANVAS + '-backdrop';
  var S_HAMBURGER = '[data-' + NAME + '-button]';
  var S_BASE = '[data-' + NAME + ']';
  var S_ELEMENT = S_BASE + ':not(.' + C_MOUNTED + ')';

  Drupal.ultimenu = Drupal.ultimenu || {};

  /**
   * Ultimenu utility functions for the main menu only.
   *
   * @param {HTMLElement} elm
   *   The ultimenu HTML element.
   */
  function process(elm) {
    var me = Drupal.ultimenu;

    // Applies to other Ultimenus.
    var elms = elm.querySelectorAll('.caret:not(.is-caret)');
    me.once(me.each(elms, function (caret) {
      $(caret).click(me.doClickCaret.bind(me));
      caret.classList.add('is-caret');
    }));

    elm.classList.add(C_MOUNTED);
  }

  /**
   * Attaches Ultimenu behavior to HTML element [data-ultimenu].
   *
   * @type {Drupal~behavior}
   */
  Drupal.behaviors.ultimenu = {
    attach: function (context) {

      var me = Drupal.ultimenu;
      context = me.context(context, S_BASE);

      var $body = $('body');
      var elms = context.querySelectorAll(S_ELEMENT);

      if (elms.length) {
        me.prepare();

        // @todo use core/once when min D9.2.
        me.once(me.each(elms, process, context));

        // Reacts on clicking Ultimenu hamburger button.
        $body.off('.uBurger').on('click.uBurger', S_HAMBURGER, me.doClickHamburger.bind(me));
        $body.off('.uBackdrop').on('click.uBackdrop', '.' + C_BACKDROP, me.triggerClickHamburger.bind(me));

        // Reacts on resizing Ultimenu.
        me.onResize(me.doResizeMain.bind(me))();
      }
    }
  };

})(jQuery, Drupal, drupalSettings);
