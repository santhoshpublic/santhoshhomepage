/**
 * @file
 * Provides AJAX functionality for Ultimenu blocks.
 */

(function ($, Drupal, drupalSettings, _win) {

  'use strict';

  var NAME = 'ultiajax';
  var C_MOUNTED = NAME + '--on';
  var S_ELEMENT = '[data-' + NAME + ']';
  var S_AJAX_CONTAINER = S_ELEMENT + ':not(.' + C_MOUNTED + ')';
  var S_AJAX_TRIGGER = '[data-' + NAME + '-trigger]';
  var SETTINGS = drupalSettings.ultimenu || {};

  Drupal.ultimenu = Drupal.ultimenu || {};

  /**
   * Ultimenu utility functions for the ajaxified links, including main menu.
   *
   * @param {HTMLElement} elm
   *   The ultimenu HTML element.
   */
  function doUltimenuAjax(elm) {
    var me = Drupal.ultimenu;
    var $elm = $(elm);
    var smw = SETTINGS.ajaxmw || null;

    if (smw && _win.matchMedia) {
      var mw = _win.matchMedia('(max-device-width: ' + smw + ')');
      if (mw.matches) {
        // Load all AJAX contents if so configured.
        $elm.find(S_AJAX_TRIGGER).each(function (i, el) {
          me.executeAjax(el);
        });

        $elm.addClass(C_MOUNTED);
        return;
      }
    }

    // Regular mobie/ desktop AJAX.
    $elm.off().on('mouseover click', S_AJAX_TRIGGER, me.triggerAjax.bind(me));
    $elm.addClass(C_MOUNTED);
  }

  /**
   * Attaches Ultimenu behavior to HTML element [data-ultiajax].
   *
   * @type {Drupal~behavior}
   */
  Drupal.behaviors.ultimenuAjax = {
    attach: function (context) {

      var me = Drupal.ultimenu;
      context = me.context(context, S_ELEMENT);

      var elms = context.querySelectorAll(S_AJAX_CONTAINER);

      if (elms.length) {
        me.once(me.each(elms, doUltimenuAjax, context));
      }
    }
  };

})(jQuery, Drupal, drupalSettings, this);
