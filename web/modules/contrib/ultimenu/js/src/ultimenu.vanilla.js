/**
 * @file
 * Provides mobile toggler for the the Ultimenu main block.
 */

(function ($, Drupal, drupalSettings, _win, _doc) {

  'use strict';

  var NAME = 'ultimenu';
  var ID_ONCE = NAME;
  var C_MOUNTED = NAME + '--on';
  var C_CANVAS = 'is-' + NAME + '-canvas';
  var C_BACKDROP = C_CANVAS + '-backdrop';
  var S_HAMBURGER = '[data-' + NAME + '-button]';
  var S_BASE = '[data-' + NAME + ']';
  var S_ELEMENT = S_BASE + ':not(.' + C_MOUNTED + ')';

  Drupal.ultimenu = Drupal.ultimenu || {};

  /**
   * Ultimenu utility functions for the main menu only.
   *
   * @param {HTMLElement} elm
   *   The ultimenu HTML element.
   */
  function process(elm) {
    var me = Drupal.ultimenu;

    // Applies to other Ultimenus.
    $.on(elm, 'click', '.caret', me.doClickCaret.bind(me));

    $.addClass(elm, C_MOUNTED);
  }

  /**
   * Attaches Ultimenu behavior to HTML element [data-ultimenu].
   *
   * @type {Drupal~behavior}
   */
  Drupal.behaviors.ultimenu = {
    attach: function (context) {

      var me = Drupal.ultimenu;
      var $body = _doc.body;
      var ctx = $.context(context, S_BASE);
      var items = $.findAll(ctx, S_ELEMENT);

      if (items.length) {
        me.prepare();

        // @todo use core/once when min D9.2.
        $.once(process, ID_ONCE, S_ELEMENT, context);

        // Reacts on clicking Ultimenu hamburger button.
        $.on($body, 'click', S_HAMBURGER, me.doClickHamburger.bind(me));
        $.on($body, 'click', '.' + C_BACKDROP, me.triggerClickHamburger.bind(me));

        // Reacts on resizing Ultimenu.
        me.onResize(me.doResizeMain.bind(me))();
      }
    },
    detach: function (context, setting, trigger) {
      if (trigger === 'unload') {
        $.once.removeSafely(ID_ONCE, S_ELEMENT, context);
      }
    }
  };

})(dBlazy, Drupal, drupalSettings, this, this.document);
