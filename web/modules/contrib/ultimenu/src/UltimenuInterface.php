<?php

namespace Drupal\ultimenu;

/**
 * Interface for Ultimenu plugins.
 */
interface UltimenuInterface {

  /**
   * Returns the config factory.
   */
  public function configFactory();

  /**
   * Returns the entity type manager.
   */
  public function entityTypeManager();

  /**
   * Returns the block manager.
   */
  public function blockManager();

  /**
   * Returns the Config Factory object.
   *
   * @param string $config
   *   The setting storage name.
   *
   * @return \Drupal\Core\Config\ConfigFactoryInterface
   *   The \Drupal\Core\Config\ConfigFactoryInterface instance.
   */
  public function config($config = 'ultimenu.settings');

  /**
   * Returns the path.
   *
   * @todo use DI when min D9.3.
   */
  public function getPath($type, $name, $absolute = FALSE);

  /**
   * Returns the Ultimenu settings.
   *
   * @param string $key
   *   The setting name.
   *
   * @return array|null
   *   The settings by its key/ name.
   */
  public function getSetting($key = NULL);

  /**
   * Return a shortcut for the default theme.
   */
  public function getThemeDefault();

  /**
   * Returns a shortcut for loading an entity.
   *
   * @param string $id
   *   The entity ID.
   * @param string $type
   *   The entity type.
   *
   * @return mixed
   *   The entity, or empty.
   */
  public function load($id, $type = 'block');

}
