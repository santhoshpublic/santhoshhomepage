<?php

namespace Drupal\ultimenu;

use Drupal\Core\Block\BlockManagerInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides Ultimenu utility methods.
 *
 * @todo extends \Drupal\blazy\BlazyBase at 3.x to DRY.
 */
abstract class UltimenuBase implements UltimenuInterface {

  use StringTranslationTrait;
  use UltimenuTrait;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Drupal\Core\Block\BlockManagerInterface.
   *
   * @var \Drupal\Core\Block\BlockManagerInterface
   */
  protected $blockManager;

  /**
   * Constructs a Ultimenu object.
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager, BlockManagerInterface $block_manager) {
    $this->configFactory = $config_factory;
    $this->entityTypeManager = $entity_type_manager;
    $this->blockManager = $block_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.block')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function entityTypeManager() {
    return $this->entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public function blockManager() {
    return $this->blockManager;
  }

  /**
   * {@inheritdoc}
   */
  public function load($id, $type = 'block') {
    return $this->entityTypeManager->getStorage($type)->load($id);
  }

  /**
   * {@inheritdoc}
   *
   * @deprecated in ultimenu:8.x-2.10 and is removed from ultimenu:3.0.0. Use
   *   self::entityTypeManager().
   * @see https://www.drupal.org/node/3364574
   */
  public function getEntityTypeManager() {
    return $this->entityTypeManager;
  }

  /**
   * {@inheritdoc}
   *
   * @deprecated in ultimenu:8.x-2.10 and is removed from ultimenu:3.0.0. Use
   *   self::blockManager() instead.
   * @see https://www.drupal.org/node/3364574
   */
  public function getBlockManager() {
    return $this->blockManager;
  }

}
