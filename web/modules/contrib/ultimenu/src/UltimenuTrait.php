<?php

namespace Drupal\ultimenu;

/**
 * A Trait common for Ultimenu split services.
 */
trait UltimenuTrait {

  /**
   * Config Factory Service Object.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * {@inheritdoc}
   */
  public function configFactory() {
    return $this->configFactory;
  }

  /**
   * {@inheritdoc}
   */
  public function config($config = 'ultimenu.settings') {
    return $this->configFactory->get($config);
  }

  /**
   * {@inheritdoc}
   */
  public function getPath($type, $name, $absolute = FALSE) {
    if ($resolver = Ultimenu::service('extension.path.resolver')) {
      $path = $resolver->getPath($type, $name);
    }
    else {
      $function = 'drupal_get_path';
      $path = is_callable($function) ? $function($type, $name) : '';
    }
    return $absolute ? \base_path() . $path : $path;
  }

  /**
   * {@inheritdoc}
   */
  public function getSetting($key = NULL) {
    return $this->config()->get($key);
  }

  /**
   * {@inheritdoc}
   */
  public function getThemeDefault() {
    return $this->configFactory->get('system.theme')->get('default');
  }

  /**
   * {@inheritdoc}
   *
   * @deprecated in ultimenu:8.x-2.10 and is removed from ultimenu:3.0.0. Use
   *   self::config() instead.
   * @see https://www.drupal.org/node/3364574
   */
  public function getConfig($config = 'ultimenu.settings') {
    return $this->config($config);
  }

}
