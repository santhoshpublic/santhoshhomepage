***
***
# <a name="roadmap"></a>ROADMAP
[x] Support multi-styled images to have various sizes, only if doable. Currently
    using CSS background images to solve sizes.  
    2/29/16

[x] Multi-breakpoint responsive grid layouts.  
    3/11/16

[x] Multi-breakpoint image styles.  
    3/14/16

[x] Supports lightbox boxes.  
    12/14/16 for Field formatter, or use Blazy for Views images.

[x] Field formatter  
    12/14/16

[x] Multimedia boxes.  
    12/14/16

[x] Decouple from Views to make GridStack a more generic layout builder.  
    2/27/2017

[x] Optional integration with core layout_discovery (core >= 8.x-3) and
    layout_plugin (core < 8.x-3) so that gridstack layouts are exposed to DS,
    Panelizer, Panels, etc.  
    3/22/2017

[x] Supports Bootstrap 4 for XL breakpoint.  
    3/29/2017

[x] A generic layout builder for Bootstrap, or Foundation.  
    3/22/2017

[x] Stamps, and rich boxes.  
    26/02/2019

[x] Fixes for broken Layout Builder integration. Since this is optional, nice to
    have, it doesn't hold back GridStack.  
    29/02/2020

[x] Native browser CSS Grid layout.  
    07/03/2020  

[x] Breaking changes with v1+.  
    15/03/2020

[x] Layout engines as plugins. Adding Material, Bulma, etc. made easy.  
    02/04/2020   

[x] Live preview for configurable colors and stylings.  
    03/05/2020  

[x] Media Library integration within Layout Builder.  
    03/05/2020  

[?] Optimization.

[!] D7 port. (WON'T FIX)

Feel free to get in touch if you'd like to chip in, or sponsor any. Thanks.
