<?php

namespace Drupal\Tests\gridstack\Kernel;

use Drupal\Tests\blazy\Kernel\BlazyKernelTestBase;
use Drupal\Tests\gridstack\Traits\GridStackKernelTrait;
use Drupal\Tests\gridstack\Traits\GridStackUnitTestTrait;

/**
 * Tests the GridStack skin manager methods.
 *
 * @coversDefaultClass \Drupal\gridstack\Skin\GridStackSkinManager
 *
 * @group gridstack
 */
class GridStackSkinManagerTest extends BlazyKernelTestBase {

  use GridStackUnitTestTrait;
  use GridStackKernelTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'system',
    'user',
    'field',
    'file',
    'filter',
    'image',
    'node',
    'text',
    'blazy',
    'gridstack',
    'gridstack_ui',
    'gridstack_test',
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installConfig([
      'field',
      'image',
      'media',
      'responsive_image',
      'node',
      'views',
      'blazy',
      'gridstack',
      'gridstack_ui',
    ]);

    $this->gridstackSkinManager = $this->container->get('gridstack.skin_manager');
  }

  /**
   * Tests cases for various methods.
   *
   * @covers ::getSkins
   * @covers ::getCache
   * @covers ::libraryInfoBuild
   */
  public function testGridStackManagerMethods() {
    $manager = $this->gridstackSkinManager;
    // Tests for skins.
    $skins = $manager->getSkins();
    $this->assertArrayHasKey('default', $skins);

    // Verify we have cached skins.
    $cid = 'gridstack_skins_data';
    $cached_skins = $manager->getCache()->get($cid);
    $this->assertEquals($cid, $cached_skins->cid);
    $this->assertEquals($skins, $cached_skins->data);

    // Verify libraries.
    $libraries = $manager->libraryInfoBuild();
    $this->assertArrayHasKey('gridstack.default', $libraries);
  }

}
