<?php

namespace Drupal\Tests\gridstack\Unit\Form;

use Drupal\gridstack_ui\Form\GridStackForm;
use Drupal\Tests\UnitTestCase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Tests the GridStack admin form.
 *
 * @coversDefaultClass \Drupal\gridstack_ui\Form\GridStackForm
 * @group gridstack
 */
class GridStackFormUnitTest extends UnitTestCase {

  /**
   * The gridstack admin service.
   *
   * @var \Drupal\gridstack\Form\GridStackAdminInterface
   */
  protected $admin;

  /**
   * The gridstack manager service.
   *
   * @var \Drupal\gridstack\GridStackManagerInterface
   */
  protected $gridstackManager;

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystem
   */
  protected $fileSystem;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->fileSystem = $this->createMock('\Drupal\Core\File\FileSystem');
    $this->admin = $this->createMock('\Drupal\gridstack\Form\GridStackAdminInterface');
    $this->gridstackManager = $this->createMock('\Drupal\gridstack\GridStackManagerInterface');
  }

  /**
   * @covers ::create
   */
  public function testGridStackFormCreate() {
    $container = $this->createMock(ContainerInterface::class);
    $exception = ContainerInterface::EXCEPTION_ON_INVALID_REFERENCE;

    $map = [
      ['file_system', $exception, $this->fileSystem],
      ['gridstack.admin', $exception, $this->admin],
      ['gridstack.manager', $exception, $this->gridstackManager],
    ];

    $container->expects($this->any())
      ->method('get')
      ->willReturnMap($map);

    $gridstackForm = GridStackForm::create($container);
    $this->assertInstanceOf(GridStackForm::class, $gridstackForm);
  }

}
