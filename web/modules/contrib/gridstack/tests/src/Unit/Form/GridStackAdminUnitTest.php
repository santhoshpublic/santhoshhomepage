<?php

namespace Drupal\Tests\gridstack\Unit\Form;

use Drupal\gridstack\Form\GridStackAdmin;
use Drupal\Tests\UnitTestCase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Tests the GridStack admin form.
 *
 * @coversDefaultClass \Drupal\gridstack\Form\GridStackAdmin
 * @group gridstack
 */
class GridStackAdminUnitTest extends UnitTestCase {

  /**
   * The blazy admin service.
   *
   * @var \Drupal\blazy\Form\BlazyAdminInterface
   */
  protected $blazyAdmin;

  /**
   * The gridstack admin service.
   *
   * @var \Drupal\gridstack\Form\GridStackAdminInterface
   */
  protected $gridstackAdmin;

  /**
   * The gridstack manager service.
   *
   * @var \Drupal\gridstack\GridStackManagerInterface
   */
  protected $gridstackManager;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->blazyAdmin = $this->createMock('\Drupal\blazy\Form\BlazyAdminInterface');
    $this->gridstackManager = $this->createMock('\Drupal\gridstack\GridStackManagerInterface');
  }

  /**
   * @covers ::create
   * @covers ::__construct
   * @covers ::blazyAdmin
   * @covers ::manager
   */
  public function testBlazyAdminCreate() {
    $container = $this->createMock(ContainerInterface::class);
    $exception = ContainerInterface::EXCEPTION_ON_INVALID_REFERENCE;

    $map = [
      // @todo use blazy.admin.formatter post blazy:2.17.
      ['blazy.admin.formatter', $exception, $this->blazyAdmin],
      ['gridstack.manager', $exception, $this->gridstackManager],
    ];

    $container->expects($this->any())
      ->method('get')
      ->willReturnMap($map);

    $gridstackAdmin = GridStackAdmin::create($container);
    $this->assertInstanceOf(GridStackAdmin::class, $gridstackAdmin);
    $this->assertInstanceOf('\Drupal\blazy\Form\BlazyAdminInterface', $gridstackAdmin->blazyAdmin());
    $this->assertInstanceOf('\Drupal\gridstack\GridStackManagerInterface', $gridstackAdmin->manager());
  }

}
