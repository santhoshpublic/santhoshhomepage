/**
 * @file
 * Provides methods for anything related to GridStack, including Outlayer.
 */

(function ($, Drupal, _win) {

  'use strict';

  var _dgs = 'data-gs-';
  var _id = 'gridstack';
  var _addClass = 'addClass';
  var _removeClass = 'removeClass';
  var _isgs = 'is-gs';
  var _blazy = Drupal.blazy || {};
  var noop = function () {};

  $.gridstack = $.gridstack || {};

  /**
   * GridStack front-end public methods.
   *
   * @namespace
   */
  $.gridstack.base = $.extend($.gridstack, {
    $el: null,
    $instance: null,
    $items: [],
    $sizer: null,
    cellHeight: 0,
    columns: null,
    itemSelector: '.' + _id + '__box:not(.is-nixbox)',
    minW: 480,
    onClass: null,
    options: {},
    vm: 0,
    windowWidth: $.windowWidth(),
    isBlazy: false,
    isLayoutBuilder: false,
    isNativeGrid: false,
    isGridStack: false,
    isUnGridStack: false,
    updateAttribute: noop,
    updateNodes: noop,
    preUpdateGrid: noop,
    postUpdateGrid: noop,
    fixAspectRatio: noop,
    destroy: noop,
    load: noop,

    column: function (el) {
      var column = $.attr(el, _dgs + 'column');
      return column ? parseInt(column, 0) : 0;
    },

    isEnabled: function (el) {
      // @todo var minW = typeof el === 'undefined' ? this.minW : parseInt(el.dataset.gsMinW);
      return this.windowWidth >= this.minW;
    },

    isValid: function (o) {
      return o && !$.isNull(o) && !$.isUnd(o);
    },

    isValidParam: function (o) {
      return o && !$.isUnd(o);
    },

    updateClasses: function (el, e) {
      var me = this;
      var enabled = me.isEnabled(el);
      var matches = el.className.match(/\d+/g);

      if (me.isValid(matches)) {
        el.className = el.className.replace(/grid-stack-(\d+)/g, '');
      }

      // At times, the library didn't set this on for some reason.
      if (me.column(el) > 0) {
        $.addClass(el, 'grid-stack-' + me.column(el));
      }

      $[enabled ? _addClass : _removeClass](el, _isgs + '-enabled');
      $[!enabled ? _addClass : _removeClass](el, _isgs + '-disabled');

      if (me.options._class) {
        $[enabled ? _addClass : _removeClass](el, me.options._class);
      }
    },

    outerHeight: function (el) {
      if (!$.isElm(el)) {
        return [];
      }
      var style = _win.getComputedStyle(el);

      return ['top', 'bottom']
        .map(function (side) {
          return parseInt(style['padding-' + side], 0);
        })
        .reduce(function (total, side) {
          return total + side;
        }, 0);
    },

    skip: function (box) {
      return $.isElm($.find(box, '.grid'));
    },

    updateRatio: function (box, el, e) {
      var me = this;

      // @todo remove check when one column mode is configurable.
      // See https://github.com/gridstack/gridstack.js/issues/304.
      if (me.skip(box)) {
        return;
      }

      var enabled = me.isEnabled(el);
      var cleaned = false;
      var stamp = $.hasClass(box, 'box--rich') || $.hasClass(box, 'box--stamp');
      var needFix = $.hasClass(box, _isgs + '-ratio');
      var defaultHeight = stamp ? 2 : 1;
      var width = enabled ? parseInt($.attr(box, 'gs-w'), 0) : 1;
      var height = enabled ? parseInt($.attr(box, 'gs-h'), 0) : defaultHeight;
      var media = $.findAll(box, '.b-noratio, .is-gs-fh');
      var content = $.find(box, '.box__content');
      var pad = Math.round(((height / width) * 100), 2);

      // This shows how problematic gapless layouts to address various contents:
      // w/o images, or just captions, or even a HTML block.
      // We might still miss a thing, but we can always work it out later.
      if ((needFix || media.length) && height > 0) {
        // Do this hack because content might be empty like blazy BG while we
        // need to catch a height. We'll remove it once done selectively as
        // needed far below.
        content.style.paddingBottom = pad + '%';
        var rectHeight = $.rect(content).height;

        // Provides the height matching its container to not collapse.
        if (media.length && (rectHeight > me.cellHeight || me.cellHeight === 'auto')) {
          $.each(media, function (medium) {
            medium.style.height = rectHeight + 'px';
            // Ensures those without this class to have it for consistent rules.
            $.addClass(medium, 'b-noratio');
          });

          // Only cleans up if we have a defined height above, else collapsed
          // such for boxes without media or images, just captions.
          // Ungridstack means no native Grid nor GridStack engine to designate
          // box dimensions, epecially the height, as otherwise collapsed. Hence
          // why the old padding hack above is still expected.
          if (!me.isUnGridStack) {
            me.cleanUpContent(content);
            cleaned = true;
          }
        }

        // Contents are mostly blocks, with likely few embedded media/ slick.
        // Padding hack is needed for disabled without contents, else collapsed.
        // Yes, native Grid is cool without padding hack as long as enabled.
        if ((!cleaned && me.isLayoutBuilder) || (!me.isUnGridStack && enabled)) {
          me.cleanUpContent(content);
        }
      }
    },

    cleanUpContent: function (content) {
      if ($.isElm(content)) {
        content.style.paddingBottom = '';
        if (!$.attr(content, 'style')) {
          $.removeAttr(content, 'style');
        }
      }
    },

    update: function (el, e) {
      var me = this;
      var slick = $.find(el, '.slick__slider');
      var splide = $.find(el, '.splide');

      me.updateSizer(el, e);

      me.updateGrid(el, e);
      me.updateClasses(el, e);

      if (!$.isUnd(e)) {
        if ($.isElm(slick) && slick.slick) {
          slick.slick.refresh();
        }
        if ($.isElm(splide) && splide.splide) {
          splide.splide.refresh();
        }
      }
    },

    updateSizer: function (el, e) {
      var me = this;
      var sizer = me.$sizer;

      if ($.isElm(sizer)) {
        // Reset to calc new dimensions based on new screen width.
        $.removeClass(sizer, _isgs + '-sized');

        // Required to determine the correct aspect ratio.
        var content = $.find(sizer, '.box__content');

        var fixRatio = function () {
          me.cellHeight = me.outerHeight(content);

          // @todo remove debug: me.$sizer.setAttribute('data-gs-ch', me.cellHeight);
          // Fixed aspect ratio earlier, before building layouts.
          // Runs it before init to pass the options to gridstack.
          // Let each layout engine fix their own problem.
          me.fixAspectRatio(el, e);
        };

        if ($.isUnd(e)) {
          fixRatio();
        }
        else {
          // Delay to support resizing. Devs still resize these days.
          // How much I hate setTimeout, it always fixes issues.
          setTimeout(function () {
            fixRatio();
          });
        }

        // Takes the sizer out of the grid flow, and internal grid calc.
        setTimeout(function () {
          $.addClass(sizer, _isgs + '-sized');

          // All two dimensional, except native, has a fixed height, remove.
          if (!me.isEnabled(el)) {
            el.style.height = '';
          }
        }, 300);
      }
    },

    buildOut: function (el) {
      var me = this;

      var doResize = function (e) {
        me.windowWidth = $.windowWidth();

        me.update(el, e);
      };

      // Two tasks: load the active grid per breakpoint, and update on resizing.
      doResize();
      // Must be faster than 100 in order to be discerned by GridStack library.
      $.on(_win, 'resize.' + _id, Drupal.debounce(doResize, 80), false);
    },

    initials: function (el) {
      var me = this;

      me.isBlazy = _blazy.init && !$.isNull(_blazy.init);
      me.isLayoutBuilder = $.hasClass(el, _isgs + '-lb');
      me.isNativeGrid = $.hasClass(el, _id + '--native');
      me.isGridStack = $.hasClass(el, _id + '--gs');
      me.isUnGridStack = $.hasClass(el, 'un' + _id);
      me.minW = parseInt($.attr(el, _dgs + 'min-w'), 0) || me.minW;
      me.vm = me.options.margin || parseInt($.attr(el, _dgs + 'vm'), 0) || 0;
      me.$items = $.findAll(el, me.itemSelector);
      me.$sizer = $.find(el, '.' + _id + '__sizer');
    },

    cleanUp: function (el) {
      var me = this;

      $.removeClass(el, _isgs + '-destroyed');
      // $.each(['config', 'min-width'], function (key) {
      // $.removeAttr(el, _dgs + key);
      // });
      if ($.isStr(me.onClass)) {
        $.addClass(el, 'is-' + me.onClass + '-on');
      }

      setTimeout(function () {
        $.removeClass(el, _isgs + '-packing');
      }, 500);
    }

  });

})(dBlazy, Drupal, this);
