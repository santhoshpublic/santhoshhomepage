/**
 * @file
 * Provides GridStack admin base utilities.
 */

(function ($, Drupal, _doc) {

  'use strict';

  var GRIDSTACK_DEFAULT_GRIDS = {
    x: 0,
    y: 0,
    w: 2,
    h: 2
  };

  $.gsa = $.gsa || {};

  /**
   * The GridStack admin base.
   */
  $.gsa.base = {
    config: {},
    options: {},
    getDefaultBox: function () {
      return GRIDSTACK_DEFAULT_GRIDS;
    },

    getModelId: function (el) {
      el = $.toElm(el);

      if ($.isElm(el)) {
        // Nested boxes are not model, looking for the root model ID.
        var root = $.closest(el, '.box--root');
        if ($.isElm(root)) {
          el = root;
        }
        return el.id;
      }
      return null;
    },

    lastBoxIndex: 0,

    isValidNode: function (node) {
      return node && (!$.isUnd(node.h) && !$.isUnd(node.w));
    },

    node: function (node) {
      var me = this;
      var box = {};

      if (me.isValidNode(node)) {
        box = {
          x: parseInt(node.x, 0),
          y: parseInt(node.y, 0),
          w: parseInt(node.w, 0),
          h: parseInt(node.h, 0)
        };

        if (node.region) {
          box.region = node.region;
        }
      }
      return box;
    },

    attributes: function (o) {
      o = o || {};

      var attributes = o;
      if ($.isUnd(o.index)) {
        return attributes;
      }

      var index = o.index;
      if (!$.isUnd(o.indexNested)) {
        var indexNested = o.indexNested;

        attributes.index = index;
        attributes.cindex = indexNested;
        attributes.pindex = index;

        attributes.mid = o.id;
        attributes.nid = index + ':' + indexNested;
      }
      else {
        attributes.index = index;
      }

      return attributes;
    },

    box: function (node, i, gid) {
      var box = node;
      var index = (i + 1);

      box.gid = gid;
      box.id = gid + '-' + index;
      box.index = index;

      return box;
    },

    oldData: function (el, isNested) {
      el = $.toElm(el);
      return $.parse(el.dataset[isNested ? 'nestedGrids' : 'previewGrids']);
    },

    newData: function (el, isNested) {
      el = $.toElm(el);

      var me = this;
      var storage = el.dataset[isNested ? 'nestedStorage' : 'storage'];
      var elStorage = $.find(_doc, '[data-drupal-selector="' + storage + '"]');
      var val = $.isElm(elStorage) ? elStorage.value : '';
      var obj = {};

      val = $.isEmpty(val) || val === '[]' ? '' : val.trim();
      if (val) {
        obj = $.parse(val);
      }

      // Ensure data can be reverted since dataStored is changing on save.
      var data = $.isEmpty(obj) ? me.oldData(el, isNested) : obj;

      // Might happen when all nodes are deleted, add one in the least on load.
      return $.isEmpty(data) ? [GRIDSTACK_DEFAULT_GRIDS] : data;
    },

    save: function (grid) {
      // Options: saveContent = true, saveGridOpt = false.
      var json = grid.save(false, false);

      return json;
    }
  };

})(dBlazy, Drupal, this.document);
