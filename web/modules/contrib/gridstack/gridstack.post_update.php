<?php

/**
 * @file
 * Post update hooks for GridStack.
 */

use Drupal\Component\Serialization\Json;
use Drupal\gridstack\GridStackDefault;

/**
 * Update deprecated settings to match v4.
 */
function gridstack_post_update_deprecated_settings_v4() {
  $config_factory = \Drupal::configFactory();

  $dep_settings = [
    'verticalMargin' => 'margin',
    'minWidth' => 'minW',
  ];

  // Removed no-longer applicable settings.
  $config = \Drupal::configFactory()->getEditable('gridstack.settings');
  $config->clear('dev');
  $config->clear('html5_ac');
  $config->save(TRUE);

  $defaults = ['bootstrap', 'default', 'foundation', 'frontend'];
  $samples = ['paz', 'tagore'];

  // Revert old default optionsets.
  foreach ($defaults as $key) {
    GridStackDefault::import('gridstack', $key);
  }

  // Revert old sample optionsets.
  if (gridstack()->moduleExists('gridstack_example')) {
    foreach ($samples as $key) {
      GridStackDefault::import('gridstack_example', $key);
    }
  }

  // Updates the rest.
  $prefix = 'gridstack.optionset.';
  foreach ($config_factory->listAll($prefix) as $name) {
    $storage = $config_factory->getEditable($name);
    $settings = $storage->get('options.settings');

    // CSS Framework has no JS settings, of course, skip.
    if (empty($settings)) {
      continue;
    }

    // Update all default 20 to the new 10.
    if (($settings['verticalMargin'] ?? FALSE) == 20) {
      $settings['verticalMargin'] = 10;
    }

    // Update deprecated settings.
    foreach ($dep_settings as $old => $new) {
      if (isset($settings[$old])) {
        $old_value = $storage->get('options.settings.' . $old);

        $storage->set('options.settings.' . $new, $old_value);
        $storage->clear('options.settings.' . $old);
      }
    }

    // Finally save it.
    $storage->save(TRUE);

    // Also update front-end string storage.
    $settings = $storage->get('options.settings');
    $storage->set('json.settings', Json::encode($settings));

    // Finally save it.
    $storage->save(TRUE);
  }

  // Clear the caches.
  \Drupal::entityTypeManager()->clearCachedDefinitions();
}

/**
 * Moved some services into folders to de-clutter.
 */
function gridstack_post_update_moved_services_into_folders() {
  // Do nothing to trigger clearing cache.
}

/**
 * Removed deprecated backbone and underscore libraries for vanilla JS.
 */
function gridstack_post_update_remove_deprecated_backbone_underscore() {
  // Do nothing to trigger clearing cache.
}

/**
 * Clear cache to change gridstack.admin service parameter.
 */
function gridstack_post_update_changed_gridstack_admin_service_parameter() {
  // Empty hook to trigger cache clear.
}
