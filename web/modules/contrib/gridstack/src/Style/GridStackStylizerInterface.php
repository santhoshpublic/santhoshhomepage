<?php

namespace Drupal\gridstack\Style;

use Drupal\gridstack\GridStackPluginManagerInterface;

/**
 * Provides an interface defining GridStack stylizer.
 */
interface GridStackStylizerInterface extends GridStackPluginManagerInterface {

  /**
   * Provides both CSS grid and js-driven attributes configurable via UI.
   */
  public function attributes(array &$attributes, array $settings): void;

  /**
   * Returns the module feature CSS classes, not available at CSS frameworks.
   */
  public function getInternalClasses(): array;

  /**
   * Returns classes that can be used for select options.
   *
   * @param bool $flatten
   *   Whether flattened or grouped.
   * @param array $framework_classes
   *   The optional framework CSS classes.
   *
   * @return array
   *   An associative array of grouped, or flattened classes.
   */
  public function getMergedClasses($flatten = FALSE, array $framework_classes = []): array;

  /**
   * Modifies item content and attributes.
   */
  public function modifyItem(
    $delta,
    array &$settings,
    array &$content,
    array &$attributes,
    array &$content_attributes,
    array $regions = []
  ): void;

  /**
   * Prepares the settings, selector and active styles.
   */
  public function prepare(
    array &$element,
    array &$attributes,
    array &$settings,
    $optionset
  ): void;

  /**
   * Builds aggregated styles based on the provided settings.
   *
   * This is not saved to public directory due to too small a portion.
   * Not a big deal, as it is always unique to an edited page, not re-usable.
   * Aside it is recommended by lighthouse relevant for the above-fold rules.
   */
  public function rootStyles(array &$element, array $styles, array $settings);

  /**
   * {@inheritdoc}
   */
  public function style(array $configuration = [], $reload = FALSE): ?object;

  /**
   * Returns a particular style from settings.
   *
   * @todo rename to ::getConfiguredStyle to avoid confusion?
   */
  public function getStyle($key, array $settings);

  /**
   * Returns a particular style from settings.
   */
  public function getConfiguredStyle($key, array $settings): ?object;

  /**
   * Returns the form builder.
   */
  public function builder(array $configuration = [], $reload = FALSE): ?object;

  /**
   * Returns the style form.
   */
  public function form(array $configuration = [], $reload = FALSE): ?object;

  /**
   * Sets the form plugin.
   */
  public function setForm(array $configuration = [], $reload = FALSE): self;

  /**
   * Parses the formatted styles per region based on settings.
   */
  public function styles(array &$attributes, array $settings): array;

}
