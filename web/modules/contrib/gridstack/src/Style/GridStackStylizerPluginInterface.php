<?php

namespace Drupal\gridstack\Style;

use Drupal\Core\Form\FormStateInterface;
use Drupal\gridstack\GridStackPluginInterface;

/**
 * Provides an interface defining GridStack styles.
 */
interface GridStackStylizerPluginInterface extends GridStackPluginInterface {

  /**
   * Returns the formatted media as Blazy CSS background.
   */
  public function buildMedia(array &$attributes, array &$settings): array;

  /**
   * Cleans up styles form.
   */
  public function cleanupStyles(array &$settings = []): void;

  /**
   * Return available entity data.
   */
  public function getEntityData($entity_form): array;

  /**
   * Returns available field media for select options.
   */
  public function getLayoutFieldOptions(): array;

  /**
   * Returns the available admin theme to fetch the media library styling.
   */
  public function getMediaLibraryTheme();

  /**
   * Sets the field name.
   */
  public function setFieldName($name): self;

  /**
   * Builds the form.
   */
  public function buildConfigurationForm(
    $optionset,
    FormStateInterface $form_state,
    array $settings,
    array $extras = []
  ): array;

  /**
   * The closing form.
   */
  public function closingForm(array &$form, array $settings): void;

  /**
   * Validates the form.
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state): void;

}
