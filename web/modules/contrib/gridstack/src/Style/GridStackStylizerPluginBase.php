<?php

namespace Drupal\gridstack\Style;

use Drupal\Core\Form\FormStateInterface;
use Drupal\gridstack\GridStackPluginBase;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides base class for all gridstack styles.
 */
abstract class GridStackStylizerPluginBase extends GridStackPluginBase implements GridStackStylizerPluginInterface {

  /**
   * The blazy entity service to support Media Library at Layout Builder pages.
   *
   * @var \Drupal\blazy\BlazyEntityInterface
   */
  protected $blazyEntity;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);

    $instance->blazyEntity = $container->get('blazy.entity');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function buildMedia(array &$attributes, array &$settings): array {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function cleanupStyles(array &$settings = []): void {
    // Do nothing.
  }

  /**
   * {@inheritdoc}
   */
  public function getEntityData($entity_form): array {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getLayoutFieldOptions(): array {
    return [];
  }

  /**
   * Returns the available admin theme to fetch the media library styling.
   */
  public function getMediaLibraryTheme() {
    $admin_theme = $this->manager->config('admin', 'system.theme');

    if ($admin_theme == 'claro') {
      return ['claro/media_library.theme', 'claro/media_library.ui'];
    }
    // Adminimal, Classy, Gin has no special media library theme, skip.
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function setFieldName($name): self {
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(
    $optionset,
    FormStateInterface $form_state,
    array $settings,
    array $extras = []
  ): array {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function closingForm(array &$form, array $settings): void {
    // Do nothing.
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state): void {
    // Do nothing.
  }

  /**
   * Returns unique variant ID.
   */
  protected function getVariantUniqueId($optionset) {
    return $optionset->id() . '__' . $optionset->randomize();
  }

}
