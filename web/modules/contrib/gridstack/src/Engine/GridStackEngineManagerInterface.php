<?php

namespace Drupal\gridstack\Engine;

use Drupal\gridstack\GridStackPluginManagerInterface;

/**
 * Provides an interface defining GridStack engines.
 */
interface GridStackEngineManagerInterface extends GridStackPluginManagerInterface {

  /**
   * Returns the active CSS framework.
   */
  public function framework(array $config = []): ?object;

  /**
   * Returns preset classes with the custom defined for .row or .box__content.
   */
  public function getClassOptions($type = 'generic', array $config = []): array;

}
