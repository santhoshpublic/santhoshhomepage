<?php

namespace Drupal\gridstack;

/**
 * Provides a base interface for GridStack plugins.
 */
interface GridStackPluginManagerInterface {

  /**
   * Provides gridstack skins and libraries.
   */
  public function attach(array &$load, array $attach, $config): void;

  /**
   * Returns cache backend service.
   */
  public function getCache();

  /**
   * Returns any config, or keyed by the $setting_name.
   *
   * @param string $key
   *   The setting key.
   * @param string $group
   *   The settings object group key.
   *
   * @return mixed
   *   The config value(s), or empty.
   */
  public function config($key = '', $group = 'gridstack.settings');

  /**
   * Returns an instance of a plugin given a plugin id.
   *
   * @param string $id
   *   The plugin id.
   * @param array $configuration
   *   The optional configuration.
   *
   * @return object
   *   Return instance of GridStack plugin.
   */
  public function load($id, array $configuration = []);

  /**
   * Returns all available plugins.
   *
   * @param array $configuration
   *   The optional configuration.
   *
   * @return object
   *   Return instances of GridStack plugin.
   */
  public function loadMultiple(array $configuration = []);

}
