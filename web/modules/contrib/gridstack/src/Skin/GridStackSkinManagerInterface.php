<?php

namespace Drupal\gridstack\Skin;

use Drupal\gridstack\GridStackPluginManagerInterface;

/**
 * Provides an interface defining GridStack skins, and asset managements.
 */
interface GridStackSkinManagerInterface extends GridStackPluginManagerInterface {

  /**
   * Returns gridstack skins registered via GridStackSkin plugin or defaults.
   */
  public function getSkins(): array;

  /**
   * Returns available skins for select options.
   */
  public function getSkinOptions(): array;

  /**
   * Implements hook_library_info_build().
   */
  public function libraryInfoBuild(): array;

}
