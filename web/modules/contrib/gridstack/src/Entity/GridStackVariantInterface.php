<?php

namespace Drupal\gridstack\Entity;

/**
 * Provides an interface defining GridStack variant entity.
 */
interface GridStackVariantInterface extends GridStackInterface {

  /**
   * Sets the variant source aka original GridStack layout.
   */
  public function setSource($source): self;

  /**
   * Returns the source GridStack optionset.
   */
  public function getOptionset($default = 'default', $custom = FALSE);

  /**
   * Creates duplicate variant.
   */
  public function createDuplicateVariant($id, $label, array $options = []);

}
