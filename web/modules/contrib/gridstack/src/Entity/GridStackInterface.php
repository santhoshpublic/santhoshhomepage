<?php

namespace Drupal\gridstack\Entity;

/**
 * Provides an interface defining GridStack entity.
 */
interface GridStackInterface extends GridStackBaseInterface {

  /**
   * Returns TRUE if Use CSS framework is enabled, else FALSE.
   */
  public function isFramework(): bool;

  /**
   * Returns the icon URI.
   */
  public function getIconUri(): ?string;

  /**
   * Returns the icon URL.
   */
  public function getIconUrl($absolute = FALSE): ?string;

  /**
   * Returns options.breakpoints.[lg|xl].[column|width|grids|nested].
   *
   * @param string $type
   *   The name of specific property: column, width, grids, or nested.
   *
   * @return mixed|array
   *   Available item by the given $type parameter, else empty.
   */
  public function getLastBreakpoint($type = 'grids');

  /**
   * Returns the current nested grids with preserved indices even if empty.
   *
   * Only cares for the last breakpoint, others inherit its structure.
   * The reason is all breakpoints may have different DOM positionings, heights
   * and widths each, but they must have the same grid structure.
   *
   * @param int $delta
   *   The current delta.
   * @param bool $preserve
   *   Whether to preserve empty grid array.
   *
   * @return mixed|array
   *   Available grids by the given $delta parameter, else empty.
   */
  public function getNestedGridsByDelta($delta = 0, $preserve = FALSE);

  /**
   * Returns options.breakpoints.[xs|sm|md|lg|xl], or all, else empty.
   *
   * If available, data may contain: column, image_style, width, grids, nested.
   *
   * @param string $breakpoint
   *   The current breakpoint: xs, sm, md, lg, xl.
   *
   * @return array
   *   Available data by the given $breakpoint parameter, else empty.
   */
  public function getBreakpoints($breakpoint = NULL): array;

  /**
   * Converts the entire breakpoint items from stored JSON into array.
   */
  public function breakpointsToArray(): array;

  /**
   * Returns the available breakpoint columns.
   */
  public function getColumns(): array;

  /**
   * Returns the last breakpoint column, or fallback to global setting.
   */
  public function getLastColumn(): int;

  /**
   * Returns the last breakpoint key: Bootstrap3/Foundation lg, the rest xl.
   */
  public function getLastBreakpointKey(): string;

  /**
   * Returns options.breakpoints.sm.[width, column, grids, nested].
   */
  public function getBreakpointItems(
    $breakpoint = 'lg',
    $type = 'grids',
    $clean = TRUE
  );

  /**
   * Returns options.breakpoints.sm.[width, column, grids, nested].
   */
  public function getBreakpointItem(
    $breakpoint = 'lg',
    $index = -1,
    $property = '',
    $type = 'grids'
  );

  /**
   * Returns grids by delta.
   */
  public function getGridsByDelta(
    $delta = 0,
    $type = 'grids',
    $preserve = FALSE
  ): array;

  /**
   * Returns property by its key and delta.
   */
  public function getGridPropertyByDelta(
    $key,
    $delta = 0,
    $type = 'grids',
    $preserve = FALSE
  );

  /**
   * Returns GridStack data as string for container attributes.
   */
  public function getData(): ?string;

  /**
   * Returns JSON for options.breakpoints[xs|sm|md|lg|xl] keyed by indices.
   *
   * Simplify and remove keys:
   * Original: [{"x":1,"y":0,"width":2,"height":8}.
   * Now: [[1,0,2,8].
   */
  public function getJsonSummaryBreakpoints(
    $breakpoint = 'lg',
    $grids = '',
    $exclude_region = TRUE
  ): ?string;

  /**
   * Returns JSON for options.breakpoints[xs|sm|md|lg|xl] keyed by indices.
   *
   * Simplify and remove keys:
   * Original: [{"x":1,"y":0,"width":2,"height":8}.
   * Now: [[1,0,2,8].
   */
  public function getJsonSummaryNestedBreakpoints(
    $breakpoint = 'lg',
    $nested = '',
    $grids = ''
  ): ?string;

  /**
   * Returns a node as required by admin storage, or frontend attributes.
   */
  public function getNode(array $grid, $exclude_region = TRUE): array;

  /**
   * Returns regions based on available grids.
   *
   * @param bool $clean
   *   The flag to exclude region containers.
   *
   * @return array
   *   Available regions, else empty.
   */
  public function prepareRegions($clean = TRUE): array;

}
