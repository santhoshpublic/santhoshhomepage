<?php

namespace Drupal\gridstack\Entity;

use Drupal\Core\Url;

/**
 * Defines the GridStack variant configuration entity.
 *
 * @ConfigEntityType(
 *   id = "gridstack_variant",
 *   label = @Translation("GridStack variant"),
 *   config_prefix = "variant",
 *   list_path = "admin/structure/gridstack/variant",
 *   entity_keys = {
 *     "id" = "name",
 *     "label" = "label",
 *     "status" = "status",
 *     "weight" = "weight",
 *     "source" = "source",
 *   },
 *   config_export = {
 *     "id",
 *     "name",
 *     "label",
 *     "description",
 *     "status",
 *     "weight",
 *     "source",
 *     "options",
 *   }
 * )
 */
class GridStackVariant extends GridStack implements GridStackVariantInterface {

  /**
   * {@inheritdoc}
   */
  public function setSource($source): self {
    $this->source = $source;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOptionset($default = 'default', $custom = FALSE) {
    $source = $this->source ? $this->source : $default;
    return GridStack::loadSafely($custom ? $default : $source);
  }

  /**
   * {@inheritdoc}
   */
  public function getOption($group, $default = NULL) {
    // @todo refine few more which should point to source as needed.
    if ($gridstack = $this->getOptionset()) {
      if ($group == 'use_framework') {
        return $gridstack->getOption($group, $default);
      }
    }
    return parent::getOption($group, $default);
  }

  /**
   * {@inheritdoc}
   */
  public function createDuplicateVariant($id, $label, array $options = []) {
    $duplicate = $this->createDuplicate();
    $duplicate->set('id', $id);
    $duplicate->set('name', $id);
    $duplicate->set('label', $label);
    $duplicate->set('options', $options);
    return $duplicate;
  }

  /**
   * {@inheritdoc}
   */
  public function toUrl($rel = 'edit-form', array $options = []) {
    $uri = NULL;
    $parameters = [];
    if ($rel === 'add-form' || $rel === 'duplicate-form') {
      $parameters['gridstack'] = $this->getOptionset()->id();

      if ($rel === 'add-form') {
        $uri = new Url("entity.gridstack_variant.add_form", $parameters);
      }
      else {
        $uri = new Url("entity.gridstack_variant.duplicate_form", $parameters);
      }
    }
    if ($rel === 'edit-form' || $rel === 'delete-form') {
      $parameters['gridstack'] = $this->getOptionset()->id();
      $parameters['gridstack_variant'] = $this->id();

      if ($rel === 'edit-form') {
        $uri = new Url("entity.gridstack_variant.edit_form", $parameters);
      }
      else {
        $uri = new Url("entity.gridstack_variant.delete_form", $parameters);
      }
    }

    if ($uri) {
      $options += [
        'language' => NULL,
        'entity_type' => 'gridstack_variant',
        'entity' => $this,
      ];

      $uri->setOptions($options);
      return $uri;
    }
    return parent::toUrl($rel, $options);
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings($group = 'settings'): array {
    $default = GridStack::load('default');
    return $default ? $default->getOptions($group) : [];
  }

  /**
   * {@inheritdoc}
   */
  public static function load($id) {
    return $id == 'default' ? GridStack::load('default') : parent::load($id);
  }

}
