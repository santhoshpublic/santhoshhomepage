<?php

namespace Drupal\gridstack\Entity;

use Drupal\blazy\Blazy;
use Drupal\blazy\Config\Entity\BlazyConfigEntityBase;
use Drupal\Component\Utility\Random;

/**
 * Defines the base class for GridStack configuration entity.
 *
 * @see \Drupal\gridstack\Entity\GridStack
 * @see \Drupal\outlayer\Entity\Outlayer
 *
 * @phpstan-ignore-next-line
 */
abstract class GridStackBase extends BlazyConfigEntityBase implements GridStackBaseInterface {

  /**
   * The administrative description.
   *
   * @var string
   */
  protected $description = '';

  /**
   * The plugin instance json to reduce frontend logic.
   *
   * @var string
   */
  protected $json = '';

  /**
   * The variant source.
   *
   * @var string
   */
  protected $source = '';

  /**
   * {@inheritdoc}
   */
  public function description(): string {
    return $this->description ?? '';
  }

  /**
   * {@inheritdoc}
   */
  public function source(): string {
    return $this->source ?? '';
  }

  /**
   * {@inheritdoc}
   */
  public function getJson($group = '') {
    $default = self::load('default');
    if ($group) {
      $defaults = $default->json[$group] ?? '';
      return $this->json[$group] ?? $defaults;
    }
    return $this->json;
  }

  /**
   * {@inheritdoc}
   */
  public function setJson($group, $value) {
    $this->json[$group] = $value;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function buildIconFileUri($dir = 'gridstack', $extension = 'png'): string {
    $path = $dir . '/' . $this->id() . '.' . $extension;
    return Blazy::normalizeUri($path);
  }

  /**
   * {@inheritdoc}
   */
  public function getIconFileUri($dir = 'gridstack', $extension = 'png'): ?string {
    $uri = $this->buildIconFileUri($dir, $extension);
    return is_file($uri) ? $uri : NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function randomize($rand = 4) {
    $random = new Random();
    return strtolower($random->name($rand));
  }

  /**
   * {@inheritdoc}
   */
  public function getLabelFromId($label) {
    $names = strpos($label, '__') === FALSE ? explode("_", $label) : explode("__", $label);
    return implode(" ", $names);
  }

  /**
   * {@inheritdoc}
   */
  public function getRandomizedId($rand = 4) {
    $id = $this->id();
    $names = strpos($id, '__') === FALSE ? explode("_", $id) : explode("__", $id);
    if (count($names) > 1) {
      array_pop($names);
    }
    return implode("__", $names) . '__' . $this->randomize($rand);
  }

}
