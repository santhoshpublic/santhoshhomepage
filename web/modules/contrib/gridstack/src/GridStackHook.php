<?php

namespace Drupal\gridstack;

use Drupal\Core\Layout\LayoutDefinition;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\gridstack\GridStackDefault as Defaults;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides GridStack utility methods for Drupal hooks.
 */
class GridStackHook {

  use StringTranslationTrait;

  /**
   * The gridstack manager service.
   *
   * @var \Drupal\gridstack\GridStackManagerInterface
   */
  protected $manager;

  /**
   * Constructs a GridStack object.
   *
   * @param \Drupal\gridstack\GridStackManagerInterface $manager
   *   The gridstack manager service.
   */
  public function __construct(GridStackManagerInterface $manager) {
    $this->manager = $manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('gridstack.manager'));
  }

  /**
   * Implements hook_config_schema_info_alter().
   *
   * @todo Also verify widget.module, and revisit if any further change.
   * @todo replace with $this->manager->configSchemaInfoAlter() post blazy:2.17.
   */
  public function configSchemaInfoAlter(array &$definitions) {
    if (isset($definitions['layout_plugin.settings'])) {
      $this->mapConfigSchemaInfoAlter($definitions['layout_plugin.settings']);
    }

    // @todo Remove when DS passes layout.settings to layout_plugin.settings.
    if (isset($definitions['core.entity_view_display.*.*.*.third_party.ds'])) {
      $this->mapConfigSchemaInfoAlter($definitions['core.entity_view_display.*.*.*.third_party.ds']['mapping']['layout']['mapping']['settings']);
    }

    foreach (['gridstack_base', 'gridstack_vanilla'] as $key) {
      if (isset($definitions[$key])) {
        $this->manager->configSchemaInfoAlter($definitions, $key, Defaults::extendedSettings());
      }
    }
  }

  /**
   * Implements hook_library_info_alter().
   */
  public function libraryInfoAlter(&$libraries, $extension) {
    if ($extension === 'gridstack') {
      $manager = $this->manager;
      if ($path = $manager->getLibrariesPath('gridstack')) {
        // $old_file = '/' . $path . '/dist/gridstack.min.js';
        $new_file = '/' . $path . '/dist/gridstack-static.js';
        $libraries['gridstack']['js'] = [$new_file => ['weight' => -2]];
      }

      if (isset($libraries['load'])) {
        $libraries['load']['dependencies'][] = 'gridstack/gridstack';
        // @fixme broken at 5.1.1:
        // $libraries['load']['dependencies'][] =
        // $manager->config('gridstatic', 'gridstack.settings')
        // ? 'gridstack/gridstatic' : 'gridstack/gridstack';
      }
    }
    elseif ($extension === 'layout_builder') {
      $libraries['drupal.layout_builder']['dependencies'][] = 'gridstack/admin_minimal';
    }
  }

  /**
   * Implements hook_field_formatter_info_alter().
   */
  public function fieldFormatterInfoAlter(array &$info) {
    $common = [
      'quickedit' => ['editor' => 'disabled'],
      'provider'  => 'gridstack',
    ];

    if ($this->manager->moduleExists('paragraphs')) {
      $info['gridstack_paragraphs'] = $common + [
        'id'          => 'gridstack_paragraphs',
        'label'       => $this->t('GridStack Paragraphs'),
        'description' => $this->t('Display the Paragraphs as a GridStack.'),
        'class'       => 'Drupal\gridstack\Plugin\Field\FieldFormatter\GridStackParagraphsFormatter',
        'field_types' => ['entity_reference_revisions'],
      ];
    }
  }

  /**
   * Implements hook_layout_alter().
   */
  public function layoutAlter(&$definitions) {
    $manager    = $this->manager;
    $optionsets = $manager->loadMultiple('gridstack');
    $excluded   = $manager->config('excludes', 'gridstack.settings');
    $framework  = $manager->config('framework', 'gridstack.settings');
    $path       = $manager->getPath('module', 'gridstack');
    $excludes   = ['default'];

    if (!empty($excluded)) {
      $excluded = strip_tags($excluded);
      $excludes = array_unique(array_merge($excludes, array_map('trim', explode(",", $excluded))));
    }

    foreach ($optionsets as $key => $optionset) {
      if (in_array($key, $excludes)) {
        continue;
      }

      $static    = !empty($framework) && $optionset->getOption('use_framework');
      $id        = $optionset->id();
      $layout_id = Defaults::layoutId($id);
      $regions   = $optionset->prepareRegions();

      // Defines the layout.
      $definition = [
        'label'          => $optionset->label(),
        'category'       => $static ? 'GridStack ' . ucwords($framework) : 'GridStack JS',
        'class'          => '\Drupal\gridstack\Plugin\Layout\GridStackLayout',
        'default_region' => 'gridstack_0',
        'icon'           => $optionset->getIconUrl(),
        'id'             => $layout_id,
        'provider'       => 'gridstack',
        'additional'     => ['optionset' => $id],
        'regions'        => $regions,
        'theme_hook'     => 'gridstack',
        'path'           => $path,
        // @fixme, no longer loaded as per D9.5.10.
        // 'library' => 'gridstack/layout',
        'config_dependencies' => [
          'config' => ['gridstack.optionset.' . $id],
          'module' => ['gridstack'],
        ],
      ];

      $definitions[$layout_id] = new LayoutDefinition($definition);
    }
  }

  /**
   * Maps config schema.
   */
  private function mapConfigSchemaInfoAlter(array &$mappings, $source = '') {
    foreach (Defaults::layoutSettings() as $key => $value) {
      $mappings2 = &$mappings['mapping'][$key];
      if (is_array($value)) {
        $mappings2['type'] = 'sequence';
        $mappings2['label'] = $key;
        $mappings2['sequence'][0]['type'] = 'mapping';
        $mappings2['sequence'][0]['label'] = $key;
      }
      else {
        $mappings2['type'] = 'string';
        $mappings2['label'] = ucwords($key);
      }
    }

    foreach (Defaults::regionSettings() as $key => $value) {
      $sequences = &$mappings['mapping']['regions']['sequence'][0]['mapping'][$key];
      if (is_array($value)) {
        $sequences['type'] = 'sequence';
        $sequences['label'] = $key;
        $sequences['sequence'][0]['type'] = 'mapping';
        $sequences['sequence'][0]['label'] = $key;
      }
      else {
        $sequences['type'] = 'string';
        $sequences['label'] = ucwords($key);
      }
    }
  }

}
