<?php

namespace Drupal\gridstack\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\gridstack\GridStackDefault;

/**
 * Plugin implementation of the 'GridStack File' to get image/ SVG from files.
 *
 * This was previously for deprecated VEF, since 1.0.8 re-purposed for SVG, WIP!
 *
 * @FieldFormatter(
 *   id = "gridstack_file",
 *   label = @Translation("GridStack File/SVG"),
 *   field_types = {
 *     "entity_reference",
 *     "file",
 *     "image",
 *     "svg_image_field",
 *   }
 * )
 *
 * @todo remove `image` at 3.x, unless dedicated for SVG (forms and displays).
 */
class GridStackFileFormatter extends GridStackFileFormatterBase {

  /**
   * {@inheritdoc}
   */
  protected static $fieldType = 'entity';

  /**
   * {@inheritdoc}
   */
  protected static $useOembed = TRUE;

  /**
   * {@inheritdoc}
   */
  protected static $useSvg = TRUE;

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return GridStackDefault::svgSettings() + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  protected function getPluginScopes(): array {
    // @todo use $this->getEntityScopes() post blazy:2.17.
    return [
      'fieldable_form'   => TRUE,
      'svg_form'         => TRUE,
      'multimedia'       => TRUE,
      'no_loading'       => TRUE,
      'no_preload'       => TRUE,
      'responsive_image' => FALSE,
    ] + parent::getPluginScopes();
  }

  /**
   * {@inheritdoc}
   */
  public static function isApplicable(FieldDefinitionInterface $field_definition) {
    $storage = $field_definition->getFieldStorageDefinition();
    return $storage->isMultiple() && $storage->getSetting('target_type') === 'file';
  }

  /**
   * {@inheritdoc}
   *
   * @todo remove post blazy:2.17.
   */
  public function buildElement(array &$element, $entity) {
    $this->blazyOembed->build($element);
  }

}
