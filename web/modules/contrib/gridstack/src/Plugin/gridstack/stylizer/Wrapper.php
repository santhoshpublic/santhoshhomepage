<?php

namespace Drupal\gridstack\Plugin\gridstack\stylizer;

use Drupal\Component\Utility\Xss;
use Drupal\Core\Form\FormStateInterface;
use Drupal\gridstack\GridStackDefault;

/**
 * Provides the range styles.
 *
 * @GridStackStylizer(
 *   id = "wrapper",
 *   label = @Translation("Wrapper")
 * )
 */
class Wrapper extends Classes {

  /**
   * {@inheritdoc}
   */
  protected function wrapperElement($optionset, FormStateInterface $form_state, array $settings, array $extras = []) {
    $context = $settings['_scope'];
    $element = [];

    $element['wrapper'] = [
      '#type'          => 'select',
      '#options'       => $context == GridStackDefault::ROOT ? GridStackDefault::mainWrapperOptions() : GridStackDefault::regionWrapperOptions(),
      '#empty_option'  => $this->t('Div'),
      '#title'         => $this->t('Wrapper'),
      '#default_value' => $settings['wrapper'],
    ];

    $element['attributes'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Attributes'),
      '#description'   => $this->t('Use comma: role|main,data-key|value'),
      '#default_value' => empty($settings['attributes']) ? '' : Xss::filter(trim($settings['attributes'] ?? '')),
    ];

    $element['wrapper_classes'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Classes'),
      '#description'   => $this->t('Use space: bg-dark text-white'),
      '#default_value' => trim($settings['wrapper_classes'] ?? ''),
    ];

    $element['row_classes'] = [
      '#type'          => 'textfield',
      '#title'         => $this->t('Row classes'),
      '#description'   => $this->t('Use space: align-items-stretch no-gutters'),
      '#default_value' => trim($settings['row_classes'] ?? ''),
      '#access'        => !empty($settings['_container']),
    ];

    return $element;
  }

}
