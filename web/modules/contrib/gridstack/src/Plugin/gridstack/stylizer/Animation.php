<?php

namespace Drupal\gridstack\Plugin\gridstack\stylizer;

use Drupal\Core\Form\FormStateInterface;

/**
 * Provides the animations.
 *
 * @GridStackStylizer(
 *   id = "animation",
 *   label = @Translation("Animation")
 * )
 */
class Animation extends StyleBase {

  /**
   * Returns utilities settings.
   */
  public static function animations() {
    return [
      'bounce',
      'flash',
      'pulse',
      'rubberBand',
      'shake',
      'headShake',
      'swing',
      'tada',
      'wobble',
      'jello',
      'bounceIn',
      'bounceInDown',
      'bounceInLeft',
      'bounceInRight',
      'bounceInUp',
      'bounceOut',
      'bounceOutDown',
      'bounceOutLeft',
      'bounceOutRight',
      'bounceOutUp',
      'fadeIn',
      'fadeInDown',
      'fadeInDownBig',
      'fadeInLeft',
      'fadeInLeftBig',
      'fadeInRight',
      'fadeInRightBig',
      'fadeInUp',
      'fadeInUpBig',
      'fadeOut',
      'fadeOutDown',
      'fadeOutDownBig',
      'fadeOutLeft',
      'fadeOutLeftBig',
      'fadeOutRight',
      'fadeOutRightBig',
      'fadeOutUp',
      'fadeOutUpBig',
      'flipInX',
      'flipInY',
      'flipOutX',
      'flipOutY',
      'lightSpeedIn',
      'lightSpeedOut',
      'hinge',
      'rollIn',
      'rollOut',
      'rotateIn',
      'rotateInDownLeft',
      'rotateInDownRight',
      'rotateInUpLeft',
      'rotateInUpRight',
      'rotateOut',
      'rotateOutDownLeft',
      'rotateOutDownRight',
      'rotateOutUpLeft',
      'rotateOutUpRight',
      'slideInDown',
      'slideInLeft',
      'slideInRight',
      'slideInUp',
      'slideOutDown',
      'slideOutLeft',
      'slideOutRight',
      'slideOutUp',
      'zoomIn',
      'zoomInDown',
      'zoomInLeft',
      'zoomInRight',
      'zoomInUp',
      'zoomOut',
      'zoomOutDown',
      'zoomOutLeft',
      'zoomOutRight',
      'zoomOutUp',
    ];
  }

  /**
   * Provides animation form elements.
   */
  protected function animationElement($optionset, FormStateInterface $form_state, array $settings, array $extras = []) {
    $element  = [];
    $context  = $settings['_scope'];
    $options  = array_combine(self::animations(), self::animations());
    $selector = '.b-gs';

    $element = [
      '#type'        => 'details',
      '#open'        => FALSE,
      '#tree'        => TRUE,
      '#title'       => $this->t('Animation'),
      '#description' => $this->t('Avoid animation options at large regions (containers) due to high CPU consumption. They are best for small regions/ containers.'),
      '#attributes'  => ['class' => ['form-wrapper--animations']],
    ];

    $animations = $settings['animations'];
    $attributes = [
      'data-gs-animation-region' => $context,
      'data-gs-target-selector'  => $selector,
    ];
    $element['animation'] = [
      '#type'          => 'select',
      '#options'       => $options,
      '#title'         => $this->t('Requires animate.css, blazy:2.1+.'),
      '#default_value' => $animations['animation'] ?? '',
      '#empty_option'  => $this->t('- None -'),
      '#attributes'    => $attributes,
    ];

    $element['animation']['#attributes']['class'][] = 'form-select--gs form-select--gs-animation';

    foreach (['duration', 'delay', 'iteration_count'] as $key) {
      $title = str_replace('_', ' ', $key);
      $css = str_replace('_', '-', $key);
      $element[$key] = [
        '#type'          => 'textfield',
        '#title'         => $this->t('@title', ['@title' => ucwords($title)]),
        '#default_value' => $animations[$key] ?? '',
        '#description'   => $this->t('E.g.: 3s for 3 seconds.'),
        '#attributes'    => $attributes,
      ];

      $element[$key]['#attributes']['class'][] = 'form-text--gs form-text--gs-' . $css;
      $element[$key]['#attributes']['data-gs-animation-key'] = $css;

      if ($key == 'iteration_count') {
        $element[$key]['#description'] = $this->t('Any number, or <code>infinite</code> to loop.');
      }
    }

    return $element;
  }

}
