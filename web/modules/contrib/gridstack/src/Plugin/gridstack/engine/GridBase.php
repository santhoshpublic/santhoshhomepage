<?php

namespace Drupal\gridstack\Plugin\gridstack\engine;

use Drupal\gridstack\Engine\GridStackEnginePluginBase;

/**
 * Provides the base class for one-dimensional layout engines.
 */
abstract class GridBase extends GridStackEnginePluginBase {

  /**
   * The colors.
   *
   * @var array
   */
  protected $colors;

  /**
   * The class options.
   *
   * @var array
   */
  protected $setClassOptions;

  /**
   * The row class options.
   *
   * @var array
   */
  protected $setRowClassOptions;

  /**
   * {@inheritdoc}
   */
  public function containerAttributes(array &$attributes, array &$settings) {
    parent::containerAttributes($attributes, $settings);

    // Provides vertical margin generic gridstackless classes.
    if ($vm = $this->getSetting('vm')) {
      $attributes['class'][] = 'is-vm vm-' . $vm;
      unset($settings['vm']);
    }
  }

  /**
   * Optimize grid widths to remove similar widths.
   */
  protected function optimizeClasses($config, array $breakpoints = []) {
    $root   = $config->get('_root', 'grids');
    $delta  = $config->get('delta', 0);
    $nid    = $config->get('nested.delta', -1);
    $unique = $region = [];

    $breakpoints = $breakpoints ?: $this->breakpoints;

    foreach (array_keys($this->sizes) as $id) {
      $item = $breakpoints[$id][$root][$delta] ?? [];

      if (empty($item)) {
        continue;
      }

      $data = $item[$nid] ?? [];
      $data = $root == 'nested' ? $data : $item;

      // Width is at 2 from x, y, width, height, region.
      if (!empty($data[2])) {
        $unique[$id] = (int) $data[2];
      }

      // Region is at 4 from x, y, width, height, region.
      if (!empty($data[4])) {
        $region[] = $data[4];
      }
    }

    $optimized = $this->unique($unique, 2);
    $optimized = $this->unique($optimized, 3);

    $column = $config->ui('optimized') ? $optimized : $unique;

    return ['column' => $column, 'region' => $region];
  }

  /**
   * Returns attempted optimized column widths per chunck.
   */
  protected function unique(array $items = [], $visible = 2) {
    $items = array_chunk($items, $visible, TRUE);
    $columns = [];
    foreach ($items as $value) {
      $columns = array_merge($columns, array_unique($value));
    }
    return $columns;
  }

  /**
   * {@inheritdoc}
   */
  public function previewOptions() {
    return [
      'background',
      'gradient',
      'rounded',
      'text_color',
      'text_transform',
    ];
  }

}
