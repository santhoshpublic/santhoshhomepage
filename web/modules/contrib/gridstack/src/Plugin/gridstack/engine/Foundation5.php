<?php

namespace Drupal\gridstack\Plugin\gridstack\engine;

/**
 * Provides a GridStack Foundation5 layout engine.
 *
 * @GridStackEngine(
 *   id = "foundation5",
 *   group = "foundation",
 *   hidden = "false",
 *   version = "5",
 *   label = @Translation("Foundation 5")
 * )
 */
class Foundation5 extends FoundationBase {

  /**
   * {@inheritdoc}
   */
  protected $colClass = 'columns';

  /**
   * {@inheritdoc}
   */
  protected $containerClasses = ['row'];

  /**
   * {@inheritdoc}
   */
  protected $nestedContainerClasses = ['row'];

}
