<?php

namespace Drupal\gridstack_ui\Form;

use Drupal\Core\Url;

/**
 * Builds the form to delete a GridStack optionset.
 */
class GridStackVariantDeleteForm extends GridStackDeleteForm {

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to delete the GridStack variant %label?', ['%label' => $this->entity->label()]);
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('entity.gridstack_variant.collection');
  }

}
